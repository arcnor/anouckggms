package com.arcnor.cpu;

public abstract class Flags {
	protected boolean[] flag = new boolean[8];
	protected static final String[] FLAG_NAMES = {
			"", "", "", "", "", "", "", ""
	};

	public Flags() {
		reset();
	}

	public void reset() {
		for (int j = 0; j < 8; j++) {
			flag[j] = false;
		}
	}

	public boolean get(int index) {
		return flag[index];
	}

	/**
	 * Returns flags as a single byte
	 */
	public int get() {
		return
				(flag[7] ? 1 : 0) << 7 |
				(flag[6] ? 1 : 0) << 6 |
				(flag[5] ? 1 : 0) << 5 |
				(flag[4] ? 1 : 0) << 4 |
				(flag[3] ? 1 : 0) << 3 |
				(flag[2] ? 1 : 0) << 2 |
				(flag[1] ? 1 : 0) << 1 |
				(flag[0] ? 1 : 0);
	}

	public void set(int index, boolean value) {
		flag[index] = value;
	}

	/**
	 * Sets flags with a single byte
	 */
	public void set(int value) {
		flag[7] = (value & 0x80) != 0;
		flag[6] = (value & 0x40) != 0;
		flag[5] = (value & 0x20) != 0;
		flag[4] = (value & 0x10) != 0;
		flag[3] = (value & 0x08) != 0;
		flag[2] = (value & 0x04) != 0;
		flag[1] = (value & 0x02) != 0;
		flag[0] = (value & 0x01) != 0;
	}

	public String getName(int index) {
		return FLAG_NAMES[index];
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("| ");
		for (int j = flag.length - 1; j >= 0; j--) {
			sb.append(flag[j] ? FLAG_NAMES[j] : "-").append(" | ");
		}
		return sb.toString().trim();
	}
}
