package com.arcnor.cpu;

public abstract class Register {
	protected int low, high;
	protected int low2, high2;
	protected final Flags f;
	private final String name;

	protected Register(final String name, final Flags f) {
		this.name = name;
		this.f = f;
	}

	public int get() {
		return (high << 8) | low;
	}

	public int getL() {
		return low;
	}

	public int getH() {
		return high;
	}

	public void set(int value) {
		low = value & 0xFF;
		high = (value & 0xFF00) >> 8;
	}

	public void setL(int value) {
		low = value;
	}

	public void setH(int value) {
		high = value;
	}

	// Swap bank
	public void ex() {
		int old = low;
		low = low2;
		low2 = old;
		old = high;
		high = high2;
		high2 = old;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return String.format("0x%1$02X (%1$d) - 0x%2$02X (%2$d)", high, low);
	}
}
