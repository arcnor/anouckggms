package com.arcnor.cpu.z80;

import com.arcnor.cpu.Flags;

public class Z80Flags extends Flags {
	static {
		FLAG_NAMES[0] = "C";
		FLAG_NAMES[1] = "N";
		FLAG_NAMES[2] = "P";
		FLAG_NAMES[3] = "X";
		FLAG_NAMES[4] = "H";
		FLAG_NAMES[5] = "Y";
		FLAG_NAMES[6] = "Z";
		FLAG_NAMES[7] = "S";
	}

	public static final int FLAG_CARRY     = 0;
	public static final int FLAG_NEG       = 1;
	public static final int FLAG_PARITY    = 2;
	public static final int FLAG_X         = 3;
	public static final int FLAG_HALFCARRY = 4;
	public static final int FLAG_Y         = 5;
	public static final int FLAG_ZERO      = 6;
	public static final int FLAG_SIGN      = 7;
}
