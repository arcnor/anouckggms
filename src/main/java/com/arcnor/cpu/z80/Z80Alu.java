package com.arcnor.cpu.z80;

import com.arcnor.cpu.Flags;

public class Z80Alu {
	private final Flags F;

	public Z80Alu(Flags F) {
		this.F = F;
	}

	public int xor(int a, int b) {
		int result = a ^ b;

		F.set(Z80Flags.FLAG_SIGN, result < 0);
		F.set(Z80Flags.FLAG_ZERO, result == 0);
		F.set(Z80Flags.FLAG_HALFCARRY, false);
		F.set(Z80Flags.FLAG_NEG, false);
		F.set(Z80Flags.FLAG_CARRY, false);
		F.set(Z80Flags.FLAG_PARITY, false);		// FIXME
		F.set(Z80Flags.FLAG_X, (b & 0x04) == 0);	// FIXME
		F.set(Z80Flags.FLAG_Y, (b & 0x10) == 0);	// FIXME

		return result;
	}

	public int or(int a, int b) {
		// FIXME?
		int result = a | b;

		F.set(Z80Flags.FLAG_SIGN, result < 0);
		F.set(Z80Flags.FLAG_ZERO, result == 0);
		F.set(Z80Flags.FLAG_HALFCARRY, false);
		F.set(Z80Flags.FLAG_NEG, false);
		F.set(Z80Flags.FLAG_CARRY, false);
		F.set(Z80Flags.FLAG_PARITY, false);		// FIXME
		F.set(Z80Flags.FLAG_X, (b & 0x04) == 0);	// FIXME
		F.set(Z80Flags.FLAG_Y, (b & 0x10) == 0);	// FIXME

		return result;
	}
}
