package com.arcnor.cpu.z80;

import com.arcnor.cpu.Flags;
import com.arcnor.cpu.Register;

public class Z80Register extends Register {

	public Z80Register(final String name, final Flags f) {
		super(name, f);
	}

	public void inc() {
		// FIXME
	}

	public void incH() {
		high++;
		// FIXME: Missing flags, overflow...
	}

	public void incL() {
		low++;
		// FIXME: Missing flags, overflow...
	}

	public void dec() {
		low--;
		if (low < 0) {
			low = 0xFF;
			high--;
		}
		// FIXME
		if (low == 0 && high == 0) {
			f.set(Z80Flags.FLAG_ZERO, true);
		}
	}

	public void decH() {
		high--;
		// FIXME: Missing flags, overflow...
		f.set(Z80Flags.FLAG_ZERO, high == 0);
	}

	public void decL() {
		low--;
		// FIXME: Missing flags, overflow...
		f.set(Z80Flags.FLAG_ZERO, low == 0);
	}
}
