package com.arcnor.cpu.z80;

import com.arcnor.cpu.debugger.JumpType;
import com.arcnor.cpu.debugger.CPUDebug;
import com.arcnor.cpu.debugger.OpcodeInfo;

public class Z80Debug implements CPUDebug<Z80> {
	// FIXME: This should be inside / related to SMSMemory.java
	static final int SYSRAM_START = 0xC000;
	static final int SYSRAM_END   = 0xE000;
	static final int SYSRAM_SIZE  = SYSRAM_END - SYSRAM_START;

	private OpcodeInfo opcodeInfo = new OpcodeInfo();

	@Override
	public OpcodeInfo decodeOpcodeInfo(int opcode) {
		opcodeInfo.opcodeSize = 1;
		opcodeInfo.dataSize = 0;

		if (opcode == Z80.OP_BIT_REG) {
			opcodeInfo.opcodeSize++;
		} else if (opcode == Z80.OP_EXTD) {
			opcodeInfo.opcodeSize++;
		} else if (opcode == Z80.OP_IX || opcode == Z80.OP_IY) {
			opcodeInfo.opcodeSize++;
		} else {
			int bit07 = opcode & 0x07;
			int bit30 = opcode & 0x30;
			int bitC0 = opcode & 0xC0;
			if (bitC0 == 0xC0) {
				// Jump & call opcodes
				if (bit07 == 0x02 || bit07 == 0x04) {
					opcodeInfo.dataSize = 2;
				} else if (bit07 == 0x06) {
					opcodeInfo.dataSize = 1;
				} else if (opcode == 0xC3 || opcode == 0xCD) {
					opcodeInfo.dataSize = 2;
				} else if (opcode == 0xD3 || opcode == 0xDB) {
					opcodeInfo.dataSize = 1;
				}
			} else if (bitC0 == 0) {
				// Jump relative & load opcodes
				if (bit07 == 0x01) {
					opcodeInfo.dataSize = 2;
				} else if (bit07 == 0x06) {
					opcodeInfo.dataSize = 1;
				} else if (bit07 == 0x00 && bit30 > 0) {
					opcodeInfo.dataSize = 1;
				} else if (bit07 == 0x02 && bit30 > 1) {
					opcodeInfo.dataSize = 2;
				}
			}
		}

		return opcodeInfo;
	}

	@Override
	public JumpType opcodeJumpType(int opcode) {
		int baseOpcode = opcode & 0xFF;

		boolean extd = (opcode & 0xFF00) == 0xED00;
		// Extended
		if (extd) {
			if ((baseOpcode & 0x05) == 0x05) {
				return JumpType.RET;    // retn / reti
			}
			return JumpType.NO_JUMP;
		}

		boolean iExtd = (opcode & 0xDF00) == 0xDD00;
		if (iExtd) {
			if (baseOpcode == 0xE9) {   // jp (i[xy])
				return JumpType.JUMP_UNKNOWN;
			}
			return JumpType.NO_JUMP;
		}
		// Normal
		switch (opcode & 0xC7) {
			case 0xC0: return JumpType.RET_COND;        // ret cond
			case 0xC2: return JumpType.JUMP_COND;       // jp cond, **
			case 0xC4: return JumpType.CALL_COND;       // call cond, **
			case 0xC7: return JumpType.CALL;            // rst **
		}
		switch (opcode) {
			case 0x18: return JumpType.JUMP_REL;            // jr *
			case 0x10: return JumpType.JUMP_REL_COND;       // djnz *
			case 0x20:
			case 0x30:
			case 0x28:
			case 0x38:
				return JumpType.JUMP_REL_COND;              // jr cond, **
			case 0x76: return JumpType.HALT;
			case 0xC3: return JumpType.JUMP;                // jp *
			case 0xC9: return JumpType.RET;
			case 0xCD: return JumpType.CALL;
			case 0xE9: return JumpType.JUMP_UNKNOWN;    // jp (hl)
		}
		return JumpType.NO_JUMP;
	}

	private static final String[] SINGLE_REG_NAMES = {"B", "C", "D", "E", "H", "L", "(HL)", "A"};
	private static final String[] PAIR_REG_NAMES = {"BC", "DE", "HL", "SP"};

	@Override
	public String opToStr(int oldPC, int op, int op2, int data, Z80 cpu) {
		String opName, comment;
		boolean jump = false;
		switch (op) {
			case Z80.OP_NOP: opName = "NOP"; comment = ""; break;
			case Z80.OP_DI: opName = "DI"; comment = "disable interrupts"; break;
			case Z80.OP_EI: opName = "EI"; comment = "enable interrupts"; break;
			case Z80.HALT: opName = "HALT"; comment = "stops the machine"; break;
			case Z80.OP_CCF: opName = "CCF"; comment = "invert carry flag"; break;
			case Z80.OP_SCF: opName = "SCF"; comment = "set carry flag"; break;
			case Z80.OP_CPL: opName = "CPL"; comment = "A's 1 compliment"; break;
			case Z80.OP_DAA: opName = "DAA"; comment = "adjusts the accumulator for BCD addition"; break;
			case Z80.OP_LD_A_VAL:
			case Z80.OP_LD_B_VAL:
			case Z80.OP_LD_C_VAL:
			case Z80.OP_LD_D_VAL:
			case Z80.OP_LD_E_VAL:
			case Z80.OP_LD_H_VAL:
			case Z80.OP_LD_L_VAL: {
				String regName = SINGLE_REG_NAMES[(op >> 3) & 0x07];
				opName = String.format("LD %1$s, %2$s", regName, format8(data, jump));
				comment = String.format("%2$s = 0x%1$02X", (byte)data, regName);
				break;
			}

			case Z80.OP_LD_HL_ADDR:
				opName = "LD (HL), " + format8(data, jump);
				comment = String.format("(HL) = 0x%1$02X ((HL) == %2$s)", (byte)data, getAddressName(cpu != null ? cpu.getRegHL().get() : -1));
				break;

			case Z80.OP_LD_HL_ADDR_A:
			case Z80.OP_LD_HL_ADDR_B:
			case Z80.OP_LD_HL_ADDR_C:
			case Z80.OP_LD_HL_ADDR_D:
			case Z80.OP_LD_HL_ADDR_E:
			case Z80.OP_LD_HL_ADDR_H:
			case Z80.OP_LD_HL_ADDR_L: {
				String regNameTo = SINGLE_REG_NAMES[op & 0x07];
				opName = String.format("LD (HL), %1$s", regNameTo);
				comment = String.format("(HL) = %1$s", regNameTo);
				break;
			}
			case Z80.OP_LD_A_HL_ADDR:
			case Z80.OP_LD_B_HL_ADDR:
			case Z80.OP_LD_C_HL_ADDR:
			case Z80.OP_LD_D_HL_ADDR:
			case Z80.OP_LD_E_HL_ADDR:
			case Z80.OP_LD_H_HL_ADDR:
			case Z80.OP_LD_L_HL_ADDR: {
				String regNameFrom = SINGLE_REG_NAMES[(op >> 3) & 0x07];
				opName = String.format("LD %1$s, (HL)", regNameFrom);
				comment = String.format("%1$s = (HL)", regNameFrom);
				break;
			}
			case Z80.OP_LD_A_A:
			case Z80.OP_LD_A_B:
			case Z80.OP_LD_A_C:
			case Z80.OP_LD_A_D:
			case Z80.OP_LD_A_E:
			case Z80.OP_LD_A_H:
			case Z80.OP_LD_A_L:

			case Z80.OP_LD_B_A:
			case Z80.OP_LD_B_B:
			case Z80.OP_LD_B_C:
			case Z80.OP_LD_B_D:
			case Z80.OP_LD_B_E:
			case Z80.OP_LD_B_H:
			case Z80.OP_LD_B_L:

			case Z80.OP_LD_C_A:
			case Z80.OP_LD_C_B:
			case Z80.OP_LD_C_C:
			case Z80.OP_LD_C_D:
			case Z80.OP_LD_C_E:
			case Z80.OP_LD_C_H:
			case Z80.OP_LD_C_L:

			case Z80.OP_LD_D_A:
			case Z80.OP_LD_D_B:
			case Z80.OP_LD_D_C:
			case Z80.OP_LD_D_D:
			case Z80.OP_LD_D_E:
			case Z80.OP_LD_D_H:
			case Z80.OP_LD_D_L:

			case Z80.OP_LD_E_A:
			case Z80.OP_LD_E_B:
			case Z80.OP_LD_E_C:
			case Z80.OP_LD_E_D:
			case Z80.OP_LD_E_E:
			case Z80.OP_LD_E_H:
			case Z80.OP_LD_E_L:

			case Z80.OP_LD_H_A:
			case Z80.OP_LD_H_B:
			case Z80.OP_LD_H_C:
			case Z80.OP_LD_H_D:
			case Z80.OP_LD_H_E:
			case Z80.OP_LD_H_H:
			case Z80.OP_LD_H_L:

			case Z80.OP_LD_L_A:
			case Z80.OP_LD_L_B:
			case Z80.OP_LD_L_C:
			case Z80.OP_LD_L_D:
			case Z80.OP_LD_L_E:
			case Z80.OP_LD_L_H:
			case Z80.OP_LD_L_L: {
				String regNameFrom = SINGLE_REG_NAMES[(op >> 3) & 0x07];
				String regNameTo = SINGLE_REG_NAMES[op & 0x07];
				opName = String.format("LD %1$s, %2$s", regNameFrom, regNameTo);
				comment = String.format("%1$s = %2$s", regNameFrom, regNameTo);
				break;
			}

			case Z80.OP_LD_BC_VAL:
			case Z80.OP_LD_DE_VAL:
			case Z80.OP_LD_HL_VAL:
			case Z80.OP_LD_SP_VAL: {
				String regName = PAIR_REG_NAMES[(op >> 4) & 0x03];
				String strData = format16(data, jump);
				opName = String.format("LD %1$s, %2$s", regName, strData);
				comment = String.format("%2$s = %1$s", strData, regName);
				break;
			}
			case Z80.OP_LD_BC_ADDR_A:
			case Z80.OP_LD_DE_ADDR_A: {
				String regName = PAIR_REG_NAMES[(op >> 4) & 0x03];
				opName = String.format("LD (%1$s), A", regName);
				comment = String.format("(%1$s) = A", regName);
				break;
			}
			case Z80.OP_XOR_A:
			case Z80.OP_XOR_B:
			case Z80.OP_XOR_C:
			case Z80.OP_XOR_D:
			case Z80.OP_XOR_E:
			case Z80.OP_XOR_H:
			case Z80.OP_XOR_L: {
				String regName = SINGLE_REG_NAMES[op & 0x07];
				opName = String.format("XOR %1$s", regName);
				comment = String.format("A = A XOR %1$s", regName);
				break;
			}
			case Z80.OP_XOR_VAL:
				opName = "XOR " + format8(data, jump); comment = "A = A XOR " + format8(data, jump); break;
			case Z80.OP_CP_VAL:
				opName = "CP " + format8(data, jump); comment = "compare A with " + format8(data, jump); break;
			case Z80.OP_SUB_VAL:
				opName = "SUB " + format8(data, jump); comment = "A = A - " + format8(data, jump); break;
			case Z80.OP_OR_A:
			case Z80.OP_OR_B:
			case Z80.OP_OR_C:
			case Z80.OP_OR_D:
			case Z80.OP_OR_E:
			case Z80.OP_OR_H:
			case Z80.OP_OR_L: {
				String regName = SINGLE_REG_NAMES[op & 0x07];
				opName = String.format("OR %1$s", regName);
				comment = String.format("A = A OR %1$s", regName);
				break;
			}
			case Z80.OP_DEC_A:
			case Z80.OP_DEC_B:
			case Z80.OP_DEC_C:
			case Z80.OP_DEC_D:
			case Z80.OP_DEC_E:
			case Z80.OP_DEC_H:
			case Z80.OP_DEC_L: {
				String regName = SINGLE_REG_NAMES[(op >> 3) & 0x07];
				opName = String.format("DEC %1$s", regName);
				comment = String.format("%1$s--", regName);
				break;
			}
			case Z80.OP_INC_A:
			case Z80.OP_INC_B:
			case Z80.OP_INC_C:
			case Z80.OP_INC_D:
			case Z80.OP_INC_E:
			case Z80.OP_INC_H:
			case Z80.OP_INC_L: {
				String regName = SINGLE_REG_NAMES[(op >> 3) & 0x07];
				opName = String.format("INC %1$s", regName);
				comment = String.format("%1$s++", regName);
				break;
			}
			case Z80.OP_ADD_A:
			case Z80.OP_ADD_B:
			case Z80.OP_ADD_C:
			case Z80.OP_ADD_D:
			case Z80.OP_ADD_E:
			case Z80.OP_ADD_H:
			case Z80.OP_ADD_L:
			case Z80.OP_ADD_HL_ADDR: {
				String regName = SINGLE_REG_NAMES[(op >> 3) & 0x07];
				opName = String.format("ADD %1$s", regName);
				comment = String.format("A = A + %1$s", regName);
				break;
			}
			case Z80.OP_ADC_A:
			case Z80.OP_ADC_B:
			case Z80.OP_ADC_C:
			case Z80.OP_ADC_D:
			case Z80.OP_ADC_E:
			case Z80.OP_ADC_H:
			case Z80.OP_ADC_L:
			case Z80.OP_ADC_HL_ADDR: {
				String regName = SINGLE_REG_NAMES[(op >> 3) & 0x07];
				opName = String.format("ADC %1$s", regName);
				comment = String.format("A = A + %1$s + CY", regName);
				break;
			}
			case Z80.OP_INC_BC:
			case Z80.OP_INC_DE:
			case Z80.OP_INC_HL:
			case Z80.OP_INC_SP: {
				String regName = PAIR_REG_NAMES[(op >> 4) & 0x03];
				opName = String.format("INC %1$s", regName);
				comment = String.format("%1$s++", regName);
				break;
			}
			case Z80.OP_DEC_BC:
			case Z80.OP_DEC_DE:
			case Z80.OP_DEC_HL:
			case Z80.OP_DEC_SP: {
				String regName = PAIR_REG_NAMES[(op >> 4) & 0x03];
				opName = String.format("DEC %1$s", regName);
				comment = String.format("%1$s--", regName);
				break;
			}
			case Z80.OP_IN_A_ADDR:
//				if (cpu != null) cpu.logComment(String.format("; Reading from %1$s to A register", getPortName(false, data)));
				opName = String.format("IN A, (%1$s)", format8(data, jump)); comment = "input from I/O to register A"; break;
			case Z80.OP_OUT_VAL_A:
//				if (cpu != null) cpu.logComment(String.format("; Writing to %1$s with A register (0x%2$02X)", getPortName(true, data), (byte) cpu.AF.getH()));
				opName = String.format("OUTA (%1$s)", format8(data, jump)); comment = "output to I/O from register A"; break;
			case Z80.OP_BIT_REG: {
				int bit = (data >> 3) & 0x07;
				int bitOp = data >> 6;
				int reg = data & 0x07;
				String regName = SINGLE_REG_NAMES[reg];
				switch (bitOp) {
					case 1:
						// BIT b, r | BIT b, (HL)
						opName = String.format("BIT %1$d, %2$s", bit, regName);
						comment = String.format("tests bit %1$d of register %2$s and sets Z flag", bit, regName);
						break;
					case 2:
						// RES b, m | RES b, (HL)
						opName = String.format("RES %1$d, %2$s", bit, regName);
						comment = String.format("resets bit %1$d of register %2$s", bit, regName);
						break;
					case 3:
						// SET b, r | SET b, (HL)
						opName = String.format("BIT %1$d, %2$s", bit, regName);
						comment = String.format("sets bit %1$d of register %2$s", bit, regName);
						break;
					default:
						throw new RuntimeException("Unknown BIT opcode");
				}
				break;
			}
			case Z80.OP_JP_NN:
				jump = true;
				opName = "JP " + format16(data, jump);
				comment = String.format("jumps (inconditionaly) to 0x%1$04X", (short)data);
				break;
			case Z80.OP_JR_E:
				jump = true;
				opName = "JR " + format8(data, jump);
				comment = String.format("jumps (relatively) to 0x%1$02X", (byte)data + oldPC + 1);
				break;
			case Z80.OP_JR_NZ:
				jump = true;
				opName = "JR NZ, " + format8(data, jump);
				comment = String.format("jumps (relatively) to 0x%1$02X if not zero", (byte)data + oldPC + 1);
				break;
			case Z80.OP_JR_Z:
				jump = true;
				opName = "JR Z, " + format8(data, jump);
				comment = String.format("jumps (relatively) to 0x%1$02X if zero", (byte)data + oldPC + 1);
				break;
			case Z80.OP_JR_NC:
				jump = true;
				opName = "JR NC, " + format8(data, jump);
				comment = String.format("jumps (relatively) to 0x%1$02X if not carry", (byte)data + oldPC + 1);
				break;
			case Z80.OP_JR_C:
				jump = true;
				opName = "JR C, " + format8(data, jump);
				comment = String.format("jumps (relatively) to 0x%1$02X if carry", (byte)data + oldPC + 1);
				break;
			case Z80.OP_JP_NZ:
				jump = true;
				opName = "JP NZ, " + format8(data, jump);
				comment = String.format("jumps to 0x%1$02X if not zero", (byte)data + oldPC + 1);
				break;
			case Z80.OP_JP_Z:
				jump = true;
				opName = "JP Z, " + format8(data, jump);
				comment = String.format("jumps to 0x%1$02X if zero", (byte)data + oldPC + 1);
				break;
			case Z80.OP_JP_NC:
				jump = true;
				opName = "JP NC, " + format8(data, jump);
				comment = String.format("jumps to 0x%1$02X if not carry", (byte)data + oldPC + 1);
				break;
			case Z80.OP_JP_C:
				jump = true;
				opName = "JP C, " + format8(data, jump);
				comment = String.format("jumps to 0x%1$02X if carry", (byte)data + oldPC + 1);
				break;
			case Z80.OP_JP_PO:
				jump = true;
				opName = "JP PO, " + format8(data, jump);
				comment = String.format("jumps to 0x%1$02X if parity odd", (byte)data + oldPC + 1);
				break;
			case Z80.OP_JP_PE:
				jump = true;
				opName = "JP PE, " + format8(data, jump);
				comment = String.format("jumps to 0x%1$02X if parity even", (byte)data + oldPC + 1);
				break;
			case Z80.OP_JP_P:
				jump = true;
				opName = "JP P, " + format8(data, jump);
				comment = String.format("jumps to 0x%1$02X if positive", (byte)data + oldPC + 1);
				break;
			case Z80.OP_JP_M:
				jump = true;
				opName = "JP M, " + format8(data, jump);
				comment = String.format("jumps to 0x%1$02X if negative", (byte)data + oldPC + 1);
				break;
			case Z80.OP_DJNZ:
				jump = true;
				opName = "DJNZ " + format8(data, jump);
				comment = String.format("jumps (relatively) to 0x%1$02X if B-- is not zero", (byte)data + oldPC + 1);
				break;
			case Z80.OP_CALL_VAL:
				jump = true;
				opName = "CALL " + format16(data, jump); comment = "calls function"; break;
			case Z80.OP_RET:
				jump = true;
				opName = "RET"; comment = "returns from function"; break;
			case Z80.OP_RST_00:
			case Z80.OP_RST_08:
			case Z80.OP_RST_10:
			case Z80.OP_RST_18:
			case Z80.OP_RST_20:
			case Z80.OP_RST_28:
			case Z80.OP_RST_30:
			case Z80.OP_RST_38: {
				int bit = op & 0x38;
				jump = true;
				opName = "RST " + format8(bit, jump); comment = "reset"; break;
			}
			case Z80.OP_RLCA:
				opName = "RLCA"; comment = "rotate A left and carry"; break;
			case Z80.OP_RRCA:
				opName = "RRCA"; comment = "rotate A right and carry"; break;
			case Z80.OP_RLA:
				opName = "RLA"; comment = "rotate A left"; break;
			case Z80.OP_RRA:
				opName = "RRA"; comment = "rotate A right"; break;
			case Z80.OP_EXTD:
				switch (op2) {
					case Z80.ED_IM0:
					case Z80.ED_IM0A:
					case Z80.ED_IM0B:
					case Z80.ED_IM0C:
						opName = "IM 0"; comment = "interrupt mode 0"; break;
					case Z80.ED_IM1:
					case Z80.ED_IM1A:
						opName = "IM 1"; comment = "interrupt mode 1"; break;
					case Z80.ED_IM2:
					case Z80.ED_IM2A:
						opName = "IM 2"; comment = "interrupt mode 2"; break;
					case Z80.ED_LDIR:
						opName = "LDIR"; comment = "transfers from (HL) to (DE), increments both, BC--, until BC == 0"; break;
					case Z80.ED_OTIR:
//						if (cpu != null) cpu.logComment(String.format("; Writing to %1$s with values TODO", getPortName(true, cpu.BC.getL())));
						opName = "OTIR"; comment = "output to I/O from registers"; break;
					case Z80.ED_RETI:
						jump = true;
						opName = "RETI"; comment = "return from interrupt"; break;
					case Z80.ED_RETN:
					case Z80.ED_RETNA:
					case Z80.ED_RETNB:
					case Z80.ED_RETNC:
					case Z80.ED_RETND:
					case Z80.ED_RETNE:
					case Z80.ED_RETNF:
						jump = true;
						opName = "RETN"; comment = "return from non maskable interrupt"; break;
					default:
						throw new RuntimeException("Unknown Extended Opcode");
				}
				break;
			default:
				throw new RuntimeException("Unknown Opcode");
		}
		return String.format("%1$-20s; %2$s", opName, comment);
	}

	@Override
	public String formatLabel(int addr) {
		return String.format("%1$04X:", addr);
	}

	private static String format8(int data, boolean jump) {
		byte byteData = (byte)data;
		return (jump && byteData < 0)
				? String.format("-%1$02Xh", -byteData)
				: String.format("%1$02Xh", byteData);
	}

	private static String format16(int data, boolean jump) {
		short shortData = (short)data;
		return (jump && shortData < 0)
				? String.format("-%1$04Xh", -shortData)
				: String.format("%1$04Xh", shortData);
	}

	private static String getAddressName(int value) {
		if (value >= SYSRAM_START && value < SYSRAM_END) {
			return String.format("SysRAM[0x%1$04X]", value - SYSRAM_START);
		} else if (value >= SYSRAM_START + SYSRAM_SIZE && value < SYSRAM_END + SYSRAM_SIZE) {
			return String.format("MirrorSysRAM[0x%1$04X]", value - SYSRAM_START + SYSRAM_SIZE);
		}
		return String.format("UNKNOWN MEMORY ADDRESS [0x%1$04X]", value);
	}

	private static String getPortName(boolean write, int port) {
		// FIXME: Mirroring is missing
		switch (port) {
			case 0x3E: return "Media port (0x3E)";
			case 0x3F: return "I/O Control port";
			case 0x7E: return write ? "(SN76489) data" : "(VDP) V counter";
			case 0x7F: return write ? "(SN76489) data (mirror)" : "(VDP) H counter";
			case 0xBE: return "(VDP) Data port";
			case 0xBF: return "(VDP) Control port";
			case 0xDC: return "I/O port A and B";
			case 0xDD: return "I/O port B and miscellaneous";
		}
		return String.format("UNKNOWN PORT (0x%1$02X)", port);
	}
}
