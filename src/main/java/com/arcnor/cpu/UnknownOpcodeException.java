package com.arcnor.cpu;

public class UnknownOpcodeException extends RuntimeException {

	public UnknownOpcodeException(int opcode, int pc) {
		super(String.format("Unknown opcode: 0x%1$02X at PC: 0x%2$08X", (byte)opcode, pc));
	}

	public UnknownOpcodeException(int opcode, int opcode2, int pc) {
		super(String.format("Unknown extended opcode: 0x%1$02X%2$02X at PC: 0x%3$08X", (byte)opcode, (byte)opcode2, pc));
	}
}
