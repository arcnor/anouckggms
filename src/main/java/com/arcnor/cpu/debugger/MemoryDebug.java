package com.arcnor.cpu.debugger;

public interface MemoryDebug {
	public String formatLabel(int addr);
	public String formatReadAddr(int addr);
	public String formatWriteAddr(int addr);
	public String commentReadAddr(int addr);
	public String commentWriteAddr(int addr);
}
