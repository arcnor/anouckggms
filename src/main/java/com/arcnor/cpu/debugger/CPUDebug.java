package com.arcnor.cpu.debugger;

import com.arcnor.system.CPUChip;

public interface CPUDebug<T extends CPUChip> {
	public OpcodeInfo decodeOpcodeInfo(int opcode);
	public JumpType opcodeJumpType(int opcode);
	public String opToStr(int oldPC, int op, int op2, int data, T cpu);
	public String formatLabel(int addr);
}
