package com.arcnor.cpu.debugger;

public enum JumpType {
	NO_JUMP,
	JUMP, JUMP_COND, JUMP_UNKNOWN,
	JUMP_REL, JUMP_REL_COND, JUMP_REL_UNKNOWN,
	CALL, CALL_COND, CALL_UNKNOWN,
	RET, RET_COND,
	HALT    // Maybe not needed...
}
