package com.arcnor.cpu.mos6502;

import com.arcnor.cpu.Flags;

public class MOS6502Flags extends Flags {
	static {
		FLAG_NAMES[0] = "C";
		FLAG_NAMES[1] = "Z";
		FLAG_NAMES[2] = "I";
		FLAG_NAMES[3] = "D";
		FLAG_NAMES[4] = "B";
		FLAG_NAMES[5] = "-";
		FLAG_NAMES[6] = "V";
		FLAG_NAMES[7] = "N";
	}

	public static final int FLAG_CARRY     = 0;
	public static final int FLAG_ZERO      = 1;
	public static final int FLAG_INTERRUPT = 2;
	public static final int FLAG_DECIMAL   = 3;
	public static final int FLAG_BREAK     = 4;
	public static final int FLAG_UNUSED    = 5;
	public static final int FLAG_OVERFLOW  = 6;
	public static final int FLAG_NEGATIVE  = 7;

	@Override
	public void reset() {
		super.reset();
		set(FLAG_INTERRUPT, true);
		set(FLAG_UNUSED, true);
	}

	@Override
	public int get() {
		return
				(flag[7] ? 1 : 0) << 7 |
				(flag[6] ? 1 : 0) << 6 |
				0x20 |
				(flag[4] ? 1 : 0) << 4 |
				(flag[3] ? 1 : 0) << 3 |
				(flag[2] ? 1 : 0) << 2 |
				(flag[1] ? 1 : 0) << 1 |
				(flag[0] ? 1 : 0);
	}

	@Override
	public void set(int value) {
		flag[7] = (value & 0x80) != 0;
		flag[6] = (value & 0x40) != 0;
		flag[4] = (value & 0x10) != 0;
		flag[3] = (value & 0x08) != 0;
		flag[2] = (value & 0x04) != 0;
		flag[1] = (value & 0x02) != 0;
		flag[0] = (value & 0x01) != 0;
	}
}
