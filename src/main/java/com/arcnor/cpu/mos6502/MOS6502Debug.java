package com.arcnor.cpu.mos6502;

import com.arcnor.cpu.debugger.JumpType;
import com.arcnor.cpu.debugger.CPUDebug;
import com.arcnor.cpu.debugger.MachineDebug;
import com.arcnor.cpu.debugger.OpcodeInfo;
import com.arcnor.cpu.debugger.ROMDebug;

public class MOS6502Debug implements CPUDebug<MOS6502> {
	private OpcodeInfo opcodeInfo = new OpcodeInfo();

	private final MachineDebug machineDebug;
	private final ROMDebug romDebug;

	public MOS6502Debug(final MachineDebug machineDebug, final ROMDebug romDebug) {
		this.machineDebug = machineDebug;
		this.romDebug = romDebug;
	}

	@Override
	public OpcodeInfo decodeOpcodeInfo(int opcode) {
		opcodeInfo.opcodeSize = 1;
		opcodeInfo.dataSize = 0;
		if ((opcode & 0x08) == 0) {
			if (opcode == MOS6502.OP_JSR) {
				opcodeInfo.dataSize = 2;
			} else if (!(opcode == MOS6502.OP_BRK || opcode == MOS6502.OP_RTI || opcode == MOS6502.OP_RTS)) {
				opcodeInfo.dataSize = 1;
			}
		} else {
			if ((opcode & 0x0C) >= 0xC) {
				opcodeInfo.dataSize = 2;
			} else if ((opcode & 0x0F) == 0x09) {
				opcodeInfo.dataSize = (opcode & 0x10) == 0 ? 1 : 2;
			}
		}
		return opcodeInfo;
	}

	@Override
	public JumpType opcodeJumpType(int opcode) {
		switch (opcode) {
			case MOS6502.OP_BCC: return JumpType.JUMP_REL_COND;
			case MOS6502.OP_BCS: return JumpType.JUMP_REL_COND;
			case MOS6502.OP_BEQ: return JumpType.JUMP_REL_COND;
			case MOS6502.OP_BMI: return JumpType.JUMP_REL_COND;
			case MOS6502.OP_BNE: return JumpType.JUMP_REL_COND;
			case MOS6502.OP_BPL: return JumpType.JUMP_REL_COND;
			case MOS6502.OP_BVC: return JumpType.JUMP_REL_COND;
			case MOS6502.OP_BVS: return JumpType.JUMP_REL_COND;
			case MOS6502.OP_JMP_AB: return JumpType.JUMP;
			case MOS6502.OP_JMP_IN: return JumpType.JUMP_UNKNOWN;
			case MOS6502.OP_JSR: return JumpType.CALL;
			case MOS6502.OP_RTI: return JumpType.RET;
			case MOS6502.OP_RTS: return JumpType.RET;
			default: return JumpType.NO_JUMP;
		}
	}

	private static final String[] OPCODE_00 = {
			null, "BIT", "JMP", "JMP", "STY", "LDY", "CPY", "CPX"
	};
	private static final String[] OPCODE_01 = {
			"ORA", "AND", "EOR", "ADC", "STA", "LDA", "CMP", "SBC"
	};
	private static final String[] OPCODE_10 = {
			"ASL", "ROL", "LSR", "ROR", "STX", "LDX", "DEC", "INC"
	};

	private static final String[] ADDRESSING_00 = {
			"#%1$s", // #immediate
			"%1$s", // zero page
			null,
			"%1$s", // absolute
			null,
			"%1$s,X", // zero page,X
			null,
			"%1$s,X", // absolute,X
	};

	private static final String[] ADDRESSING_01 = {
			"(%1$s,X)", // (zero page,X)
			"%1$s", // zero page
			"#%1$s", // #immediate
			"%1$s", // absolute
			"(%1$s),Y", // (zero page),Y
			"%1$s,X", // zero page,X
			"%1$s,Y", // absolute,Y
			"%1$s,X", // absolute,X
	};

	private static final String[] ADDRESSING_10 = {
			"#%1$s", // #immediate
			"%1$s", // zero page
			"A", // accumulator
			"%1$s", // absolute
			null,
			"%1$s,X", // zero page,X
			null,
			"%1$s,X", // absolute,X
	};

	private static final String[] BRANCH_NAMES = {
			"BPL", "BMI", "BVC", "BVS", "BCC", "BCS", "BNE", "BEQ"
	};
	private static final String[] BRANCH_FLAGS = {
			"negative", "overflow", "carry", "zero"
	};


	@Override
	public String opToStr(int oldPC, int op, int op2, int data, MOS6502 cpu) {
		String opName = null, comment = null;
		switch (op) {
			case MOS6502.OP_BRK:
				opName = "BRK"; comment = "Simulate Interrupt ReQuest"; break;
			case MOS6502.OP_JSR:
				opName = String.format("JSR %1$s", formatLabel(data)); comment = "Jump to SubRoutine"; break;
			case MOS6502.OP_JMP_IN: {
				String dataStr = format16(data, false);
				opName = String.format("JMP (%1$s)", dataStr); comment = "Jump to value pointed by " + dataStr; break;
			}

			case MOS6502.OP_STA_0: opName = String.format("STA %1$s", formatWriteAddr8(data, false)); comment = ""; break;
			case MOS6502.OP_STA_AB: opName = String.format("STA %1$s", formatWriteAddr16(data, false)); comment = ""; break;
			case MOS6502.OP_STX_0: opName = String.format("STX %1$s", formatWriteAddr8(data, false)); comment = ""; break;
			case MOS6502.OP_STX_AB: opName = String.format("STX %1$s", formatWriteAddr16(data, false)); comment = ""; break;
			case MOS6502.OP_STY_0: opName = String.format("STY %1$s", formatWriteAddr8(data, false)); comment = ""; break;
			case MOS6502.OP_STY_AB: opName = String.format("STY %1$s", formatWriteAddr16(data, false)); comment = ""; break;
			case MOS6502.OP_LDA_0: opName = String.format("LDA %1$s", formatReadAddr8(data, false)); comment = ""; break;
			case MOS6502.OP_LDA_AB: opName = String.format("LDA %1$s", formatReadAddr16(data, false)); comment = ""; break;
			case MOS6502.OP_LDX_0: opName = String.format("LDX %1$s", formatReadAddr8(data, false)); comment = ""; break;
			case MOS6502.OP_LDX_AB: opName = String.format("LDX %1$s", formatReadAddr16(data, false)); comment = ""; break;
			case MOS6502.OP_LDY_0: opName = String.format("LDY %1$s", formatReadAddr8(data, false)); comment = ""; break;
			case MOS6502.OP_LDY_AB: opName = String.format("LDY %1$s", formatReadAddr16(data, false)); comment = ""; break;

			case MOS6502.OP_RTI:
				opName = "RTI"; comment = "ReTurn from Interrupt"; break;
			case MOS6502.OP_RTS:
				opName = "RTS"; comment = "ReTurn from Subroutine"; break;
			case MOS6502.OP_PHP:
				opName = "PHP"; comment = "PusH P onto Stack"; break;
			case MOS6502.OP_PLP:
				opName = "PLP"; comment = "PulL from Stack to P"; break;
			case MOS6502.OP_PHA:
				opName = "PHA"; comment = "PusH A onto Stack"; break;
			case MOS6502.OP_PLA:
				opName = "PLA"; comment = "PulL from Stack to A"; break;
			case MOS6502.OP_INX:
				opName = "INX"; comment = "Increment X by one"; break;
			case MOS6502.OP_INY:
				opName = "INY"; comment = "Increment Y by one"; break;
			case MOS6502.OP_DEX:
				opName = "DEX"; comment = "Decrement X by one"; break;
			case MOS6502.OP_DEY:
				opName = "DEY"; comment = "Decrement Y by one"; break;
			case MOS6502.OP_TAX:
				opName = "TAX"; comment = "Transfer A to X"; break;
			case MOS6502.OP_TXA:
				opName = "TXA"; comment = "Transfer X to A"; break;
			case MOS6502.OP_TAY:
				opName = "TAY"; comment = "Transfer A to Y"; break;
			case MOS6502.OP_TYA:
				opName = "TYA"; comment = "Transfer Y to A"; break;
			case MOS6502.OP_TXS:
				opName = "TXS"; comment = "Transfer X to Stack Pointer"; break;
			case MOS6502.OP_TSX:
				opName = "TSX"; comment = "Transfer Stack Pointer to X"; break;
			case MOS6502.OP_CLC:
				opName = "CLC"; comment = "Clear Carry Flag"; break;
			case MOS6502.OP_SEC:
				opName = "SEC"; comment = "Set Carry flag"; break;
			case MOS6502.OP_CLI:
				opName = "CLI"; comment = "Clear Interrupt (enable) Flag"; break;
			case MOS6502.OP_SEI:
				opName = "SEI"; comment = "Set Interrupt (disable) Flag"; break;
			case MOS6502.OP_CLV:
				opName = "CLV"; comment = "Clear oVerflow Flag"; break;
			case MOS6502.OP_CLD:
				opName = "CLD"; comment = "Clear Decimal Flag"; break;
			case MOS6502.OP_SED:
				opName = "SED"; comment = "Set Binary Coded Decimal Flag"; break;
			case MOS6502.OP_NOP:
				opName = "NOP"; comment = "No OPeration"; break;
		}
		if (opName == null) {
			int a = (op & 0xE0) >> 5;
			if ((op & 0x1F) == 0x10) {
				// Branches
				int flag = (op & 0xC0) >> 6;
				boolean set = (op & 0x20) != 0;
				int addr = ((byte)data) + oldPC + 2;
				opName = BRANCH_NAMES[a] + ' ' + formatLabel(addr);
				comment = String.format("branch to %1$s if %2$s is %3$s", format16(addr, false), BRANCH_FLAGS[flag], set ? "set" : "not set");
			} else {
				int b = (op & 0x1C) >> 2;
				int c = op & 0x03;
				String dataStr = (b == 3 || b == 6 || b == 7) ? format16(data, false) : format8(data, false);
				switch (c) {
					case 0x00:
						opName = String.format(OPCODE_00[a] + ' ' + ADDRESSING_00[b], dataStr);
						comment = "";
						break;
					case 0x01:
						opName = String.format(OPCODE_01[a] + ' ' + ADDRESSING_01[b], dataStr);
						comment = "";
						break;
					case 0x02: {
						String addrStr = ADDRESSING_10[b];
						if (b == 5 && (a == 4 || a == 5)) {     // STX & LDX with "zero page,X" is "zero page,Y"
							addrStr = "%1$s,Y";
						} else if (b == 7 && a == 5) {          // LDX with "absolute,X" is "absolute,Y"
							addrStr = "%1$s,Y";
						}
						opName = String.format(OPCODE_10[a] + ' ' + addrStr, dataStr);
						comment = "";
						break;
					}
				}
			}
		}
		return String.format("%1$-20s; %2$s", opName, comment);  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public String formatLabel(int addr) {
		String lbl = romDebug != null ? romDebug.formatLabel(addr) : null;
		if (lbl == null) {
			lbl = machineDebug.formatLabel(addr);
		}
		return lbl != null ? lbl : String.format("L%1$04X", addr);
	}

	public String formatReadAddr8(int addr, boolean jump) {
		final String lbl = machineDebug.formatReadAddr(addr);
		return lbl != null ? lbl : format8(addr, jump);
	}

	public String formatReadAddr16(int addr, boolean jump) {
		final String lbl = machineDebug.formatReadAddr(addr);
		return lbl != null ? lbl : format16(addr, jump);
	}

	public String formatWriteAddr8(int addr, boolean jump) {
		final String lbl = machineDebug.formatWriteAddr(addr);
		return lbl != null ? lbl : format8(addr, jump);
	}

	public String formatWriteAddr16(int addr, boolean jump) {
		final String lbl = machineDebug.formatWriteAddr(addr);
		return lbl != null ? lbl : format16(addr, jump);
	}

	private static String format8(int data, boolean jump) {
		byte byteData = (byte)data;
		return (jump && byteData < 0)
				? String.format("-$%1$02X", -byteData)
				: String.format("$%1$02X", byteData);
	}

	private static String format16(int data, boolean jump) {
		short shortData = (short)data;
		return (jump && shortData < 0)
				? String.format("-$%1$04X", -shortData)
				: String.format("$%1$04X", shortData);
	}
}
