package com.arcnor.vdp;

import com.arcnor.system.MemoryChip;
import com.arcnor.system.VideoChip;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;

public class TMS9918 implements VideoChip {
	public static final int SCREEN_W = 256;
	public static final int SCREEN_H = 192;
	public static final int SCREEN_CHAR_W = SCREEN_W / 8;
	public static final int SCREEN_CHAR_H = SCREEN_H / 8;

	private final JPanel video;
	private final BufferedImage videoSurface;
	private static final long FPS_INTERVAL = 1000 / 60;
	private final byte[] regs = new byte[0x08];
	private final MemoryChip mem;

	private static int[] PALETTE = {
		0x000000,
		0x000000,
		0x3EB849,
		0x74D07D,
		0x5955E0,
		0x8076F1,
		0xB95E51,
		0x65DBEF,
		0xDB6559,
		0xFF897D,
		0xCCC35E,
		0xDED087,
		0x3AA241,
		0xB766B5,
		0xCCCCCC,
		0xFFFFFF,
	};

	public static final int REG_0_FLAG_M2     = 0x02;
	public static final int REG_0_FLAG_EXTVID = 0x01;

	public static final int REG_1_FLAG_16K  = 0x80;
	public static final int REG_1_FLAG_BL   = 0x40;
	public static final int REG_1_FLAG_GINT = 0x20;
	public static final int REG_1_FLAG_M1   = 0x10;
	public static final int REG_1_FLAG_M3   = 0x08;
	public static final int REG_1_FLAG_SI   = 0x02;
	public static final int REG_1_FLAG_MAG  = 0x01;

	public static final int REG_2_MASK_PN = 0x0F;   // Pattern name table

	public static final int REG_3_MASK_CT = 0xFF;   // Color table

	public static final int REG_4_MASK_PG = 0x07;   // Pattern generator table

	public static final int REG_5_MASK_SA = 0x7F;   // Sprite attributes table

	public static final int REG_6_MASK_SG = 0x07;   // Sprite generator table

	public static final int REG_7_MASK_TC = 0xF0;   // Text color
	public static final int REG_7_MASK_BD = 0x0F;   // Backdrop color

	public TMS9918(MemoryChip mem) {
		this.mem = mem;

		JFrame frame = new JFrame();
		video = new JPanel();

		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gs = ge.getDefaultScreenDevice();
		GraphicsConfiguration gc = gs.getDefaultConfiguration();

		videoSurface = gc.createCompatibleImage(SCREEN_W, SCREEN_H, Transparency.OPAQUE);

		frame.add(video);
		frame.setSize(SCREEN_W, SCREEN_H);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.setVisible(true);

		SwingWorker sw = new SwingWorker() {
			@Override
			protected Object doInBackground() throws Exception {
				long start = System.currentTimeMillis();
				long end;
				while (video.isDisplayable()) {
					while ((end = System.currentTimeMillis()) < start + FPS_INTERVAL) {
						Thread.sleep(1);
					}
					start = end;
					publish();
				}
				return null;
			}

			@Override
			protected void process(List chunks) {
				draw();
				Graphics2D g2 = (Graphics2D) video.getGraphics();
				g2.drawImage(videoSurface, 0, 0, null);
				g2.dispose();
			}
		};
		sw.execute();
	}

	private void draw() {
		// Mode 0 (GRAPHICS 1)
		int count = 0;
		int pnAddr = (regs[0x02] & REG_2_MASK_PN) << 10;
		int ctAddr = regs[0x03] << 6;
		int pgAddr = (regs[0x04] & REG_4_MASK_PG) << 11;
		for (int y = 0; y < SCREEN_H; y += 8) {
			for (int x = 0; x < SCREEN_W; x += 8) {
				byte ch = mem.readSigned(pnAddr++);
				byte col = mem.readSigned(ctAddr + (ch >> 3));
				int foreCol = PALETTE[col >> 4];
				int backCol = PALETTE[col & 0x0F];

				for (int j = 0; j < 8; j++) {
					byte pat = mem.readSigned(pgAddr + (ch << 3) + j);
					int mask = 0x80;
					for (int k = 0; k < 8; k++) {
						videoSurface.setRGB(x + k, y + j, (pat & mask) == 0 ? backCol : foreCol);
						mask >>= 1;
					}
				}
			}
		}
	}

	// Write Steps
	// Step 0: Address / Data writing
	// Step 1: Mode decoding and setup
	// Step three (only on modes 0 and 1): Data read/write
	private boolean pairFlag = false;
	private byte tempByte = 0;
	private int addr = 0;

	private byte status = 0;

	public static final int FLAG_STATUS_INT     = 0x80;
	public static final int FLAG_STATUS_5S      = 0x40;
	public static final int FLAG_STATUS_C       = 0x20;
	public static final int FLAG_STATUS_5S_NUM  = 0x1F;

	public void write(byte b, int port) {
		if ((port & 0x01) == 0) {   // Port 0
			mem.write(addr, b);
			addr = (addr + 1) % 0x3FFF; // Overflow
		} else {                    // Port 1
			if (!pairFlag) {
				tempByte = b;
				pairFlag = true;
				return;
			} else {
				switch (b & 0xC0) {
					case 0x80:
						// Write to VDP register
						regs[b & 0x07] = tempByte;
						break;
					case 0x00:
					case 0x40:
						// Write/Read to/From VRAM register
						addr = ((b & 0x3F) << 8) | tempByte;
						break;
				}
			}
		}
		pairFlag = false; // Flag reset after data is written, or after control port is written
	}

	public byte read(byte b, int port) {
		// Reset pairFlag on every read
		pairFlag = false;

		if ((port & 0x01) == 1) {       // Port 1
			byte oldStatus = status;
			status = (byte)(status & 0x5F); // Reset INTerrupt and Coincidence flags
			return oldStatus;
		}
		// Port 0
		byte value = mem.readSigned(addr);
		addr = (addr + 1) % 0x3FFF;
		return value;
	}
}
