package com.arcnor.system;

public interface MemoryChip {
	public byte readSigned(int addr);
	public int readUnsigned(int addr);
	public void write(int addr, byte value);

	public byte[] getData(int addr, int length);

	/**
	 * The name of the memory regions in this chip
	 */
	public String[] getRegionNames();

	/**
	 * The size of the specified region
	 */
	public long getRegionLength(int region);

	/**
	 * The offset relative to the specified region
	 */
	public long getRegionOffset(int region, long offset);
}
