package com.arcnor.system;

import com.arcnor.cpu.Flags;
import com.arcnor.cpu.Register;

public interface CPUChip {
	/**
	 * Performs a reset of the processor. Registers, PC and any peripheral should be set at initial values
	 */
	public void reset();

	/**
	 * Process a full cycle (fetch, decode and execute)
	 */
	public void doCycle();

	/**
	 * The memory wired to this CPU.
	 */
	public MemoryChip getMemory();

	/**
	 * The current clock cycles
	 */
	public long getClockCycles();

	/**
	 * The address of the Non Maskable Interrupt on memory
	 */
	public long getNMIAddr();

	/**
	 * The address of Power On Reset location on memory
	 */
	public long getPORAddr();

	/**
	 * The address(es) of the Interrupt handler(s) on memory
	 */
	public long[] getINTAddrs();

	/**
	 * The Program Counter
	 */
	public int getPC();

	/**
	 * The Flag register
	 */
	public Flags getFlags();

	/**
	 * The number of registers this processor has
	 */
	public int getNumRegisters();

	/**
	 * Retrieves the register with index idx. Registers from 0 to {@link #getNumRegisters()} should exist
	 */
	public Register getRegister(int idx);

	/**
	 * The number of counters (like PC and SP)
	 */
	public int getNumCounters();

	/**
	 * The value of the counter with index idx. Counters from 0 to {@link #getNumCounters()} should exist
	 */
	public int getCounter(int idx);

	/**
	 * The name of the counter with index idx. Counters from 0 to {@link #getNumCounters()} should exist
	 */
	public String getCounterName(int idx);

	/**
	 * The size of the counter with index idx <strong>(in nibbles)</strong>. Counters from 0 to {@link #getNumCounters()} should exist
	 */
	public int getCounterSize(int idx);

	/**
	 * Sets the value of the counter with index idx. Counters from 0 to {@link #getNumCounters()} should exist
	 */
	public void setCounter(int idx, int value);
}
