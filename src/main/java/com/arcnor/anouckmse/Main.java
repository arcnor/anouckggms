package com.arcnor.anouckmse;

import com.arcnor.anouckmse.debugger.Debugger;
import com.arcnor.anouckmse.machine.atari2600.Atari2600Memory;
import com.arcnor.anouckmse.machine.c64.C64Debug;
import com.arcnor.anouckmse.machine.c64.C64Memory;
import com.arcnor.anouckmse.machine.nes.NESMemory;
import com.arcnor.anouckmse.machine.sms.SMSMemory;
import com.arcnor.anouckmse.swing.Theming;
import com.arcnor.cpu.debugger.ROMDebug;
import com.arcnor.cpu.mos6502.MOS6502;
import com.arcnor.cpu.mos6502.MOS6502Debug;
import com.arcnor.cpu.z80.Z80;
import com.arcnor.cpu.z80.Z80Debug;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class Main {
	public static void main(String[] args) throws IOException {
		Theming.loadLAF();

//		File f = new File("bios/bios13fx.sms");
//		RandomAccessFile f = new RandomAccessFile("roms/Only Words/owdemo.sms", "r");
//		File f = new File("roms/Genesis 6 Button Controller Test by Charles MacDonald (PD).sms");
//		RandomAccessFile f = new RandomAccessFile("roms/Atari2600/Basic_Background.bin", "r");
		RandomAccessFile f = new RandomAccessFile("roms/NES/nestest.nes", "r");

		byte[] buf = new byte[(int)f.length()];
		f.read(buf);
		f.close();

		ROMDebug romDebug = new ROMDebug() {
			@Override
			public String formatLabel(int addr) {
				switch (addr) {
					case 0xC72D: return "BranchTests";
					case 0xC7DB: return "FlagTests";
					case 0xC885: return "ImmediateTests";
					case 0xCBDE: return "ImpliedTests";
					case 0xCDF8: return "StackTests";
					case 0xCEEE: return "AccumulatorTests";
					case 0xCFA2: return "(I,X)_Tests";
					case 0xD174: return "0P_Tests";
					case 0xD4FB: return "Abs_Tests";
					case 0xD900: return "(I),Y_Tests";
					case 0xDAE0: return "(I),Y_Tests2";
					case 0xDF4A: return "Abs,Y_Tests";
					case 0xDBB8: return "0P,X_Tests";
					case 0xE1AA: return "Abs,X_Tests";
					case 0xC6A3: return "InvalidNOPTests";
					case 0xE51E: return "InvalidLAXTests";
					case 0xE73D: return "InvalidSAXTests";
					case 0xE8D3: return "InvalidSBCTests";
					case 0xE916: return "InvalidDCPTests";
					case 0xEB86: return "InvalidISBTests";
					case 0xEDF6: return "InvalidSLOTests";
					case 0xF066: return "InvalidRLATests";
					case 0xF2D6: return "InvalidSRETests";
					case 0xF546: return "InvalidRRATests";
					case 0xC66B: return "TestsOK";
					case 0xC66F: return "TestsNOT_OK";
				}
				return null;
			}

			@Override
			public String formatReadAddr(int addr) {
				return null;
			}

			@Override
			public String formatWriteAddr(int addr) {
				return null;
			}

			@Override
			public String commentReadAddr(int addr) {
				return null;
			}

			@Override
			public String commentWriteAddr(int addr) {
				return null;
			}
		};

		/*final String[] roms = { "bios/C64/chargen", "bios/C64/basic", "bios/C64/kernal" };
		final byte[][] bufs = new byte[3][];

		int idx = 0;
		for (String rom : roms) {
			RandomAccessFile f = new RandomAccessFile(rom, "r");

			byte[] buf = new byte[(int)f.length()];
			f.read(buf);
			f.close();

			bufs[idx++] = buf;
		}*/

//		final Z80 z80 = new Z80(new SMSMemory(buf));
//		Debugger debugger = new Debugger(z80, new Z80Debug());
//		final MOS6502 mos6502 = new MOS6502(new Atari2600Memory(buf));
//		final MOS6502 mos6502 = new MOS6502(new C64Memory(bufs[0], bufs[1], bufs[2]));
		final MOS6502 mos6502 = new MOS6502(new NESMemory(buf));
		Debugger debugger = new Debugger(mos6502, new MOS6502Debug(new C64Debug(), romDebug));
		debugger.setVisible(true);
	}
}
