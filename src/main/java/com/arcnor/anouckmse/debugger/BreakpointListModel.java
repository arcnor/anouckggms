package com.arcnor.anouckmse.debugger;

import com.arcnor.anouckmse.swing.MutableListModel;
import javax.swing.AbstractListModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class BreakpointListModel<T extends Number> extends AbstractListModel implements MutableListModel<T> {
	private final List<T> breakpoints = new ArrayList<T>();
	private final Comparator<T> SIMPLE_COMP = new Comparator<T>() {
		@Override
		public int compare(T o1, T o2) {
			return o1.intValue() - o2.intValue();
		}
	};

	@Override
	public int getSize() {
		return breakpoints.size();
	}

	@Override
	public T getElementAt(int index) {
		return breakpoints.get(index);
	}

	@Override
	public boolean add(T obj) {
		if (breakpoints.contains(obj)) {
			return false;
		}
		boolean result = breakpoints.add(obj);
		if (result) {
			Collections.sort(breakpoints, SIMPLE_COMP);
			int pos = breakpoints.indexOf(obj);
			fireIntervalAdded(this, pos, pos);
		}
		return result;
	}

	@Override
	public boolean contains(T obj) {
		return breakpoints.contains(obj);
	}

	@Override
	public boolean remove(T obj) {
		int idx = breakpoints.indexOf(obj);
		boolean result = breakpoints.remove(obj);
		if (result) {
			fireIntervalRemoved(this, idx, idx);
		}
		return result;
	}

	@Override
	public boolean remove(int index) {
		T result = breakpoints.remove(index);
		if (result != null) {
			fireIntervalRemoved(this, index, index);
		}
		return result != null;
	}
}
