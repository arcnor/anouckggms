package com.arcnor.anouckmse.debugger;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ScrollPaneConstants;

import java.awt.Adjustable;
import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

class VisualRegionScroller extends JPanel {
	private final ASMTableModel asmModel;
	private final int orientation;
	private final Color COLOR_POS = new Color(0x00, 0xFF, 0x00, 0x80);
	private Map<Integer, Color> colorCache = new HashMap<Integer, Color>();
	private int pos, posSize;
	private final VisualScrollArea scrollArea;

	public VisualRegionScroller(int orientation, ASMTableModel asmModel, final JScrollBar asmDisplayVertScroll) {
		super(new BorderLayout());

		setPreferredSize(new Dimension(
				orientation == JScrollBar.HORIZONTAL ? -1 : 50,
				orientation == JScrollBar.HORIZONTAL ? 50 : -1
		));

		this.orientation = orientation;
		this.asmModel = asmModel;

		colorCache.put(0, Color.BLACK);

		scrollArea = new VisualScrollArea(asmDisplayVertScroll);

		final JScrollPane scrollPane = new JScrollPane(scrollArea);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		add(scrollPane, BorderLayout.CENTER);

		scrollArea.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JPanel panel = (JPanel) e.getSource();
				double relPos = (double) e.getX() / panel.getWidth();
				asmDisplayVertScroll.setValue((int) (asmDisplayVertScroll.getMaximum() * relPos) + 1);
			}
		});

		final JPanel pnlInput = new JPanel();
		pnlInput.setLayout(new BorderLayout());
		Action zoomIn = new AbstractAction("+") {
			@Override
			public void actionPerformed(ActionEvent e) {
				scrollArea.incZoom();
			}
		};
		Action zoomOut = new AbstractAction("-") {
			@Override
			public void actionPerformed(ActionEvent e) {
				scrollArea.decZoom();
			}
		};
		Action zoomAuto = new AbstractAction("1:1") {
			@Override
			public void actionPerformed(ActionEvent e) {
				scrollArea.resetZoom();
			}
		};
		pnlInput.add(createSmallButton(zoomIn), orientation == JScrollBar.HORIZONTAL ? BorderLayout.NORTH : BorderLayout.WEST);
		pnlInput.add(createSmallButton(zoomAuto), BorderLayout.CENTER);
		pnlInput.add(createSmallButton(zoomOut), orientation == JScrollBar.HORIZONTAL ? BorderLayout.SOUTH : BorderLayout.EAST);
		add(pnlInput, orientation == JScrollBar.HORIZONTAL ? BorderLayout.EAST : BorderLayout.NORTH);
	}

	private Component createSmallButton(Action act) {
		JButton result = new JButton(act);
		result.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 4));
		return result;
	}

	private class VisualScrollArea extends JPanel implements AdjustmentListener {
		private final ASMTableModel asmModel = VisualRegionScroller.this.asmModel;
		private final JScrollBar asmDisplayVertScroll;

		private final int orientation = VisualRegionScroller.this.orientation;
		private int zoom = 1;
		private int normalWidth = -1;
		private JViewport scrollPane;

		public VisualScrollArea(JScrollBar asmDisplayVertScroll) {
			this.asmDisplayVertScroll = asmDisplayVertScroll;
			asmDisplayVertScroll.addAdjustmentListener(this);
		}

		@Override
		protected void paintComponent(Graphics g) {
			Graphics2D g2 = (Graphics2D) g.create();

			int rowNum = asmModel.getRowCount();
			final Rectangle bounds = getBounds();
			final Rectangle clip = g2.getClipBounds();
			if (orientation == JScrollBar.HORIZONTAL) {
				float ratio = (float)rowNum / bounds.width;
				double colorRatio = ratio / 255f;
				for (int j = clip.x; j < (clip.x + clip.width); j++) {
					final int rowIndex = (int) (j * ratio);
					int lineColor = 0;
					for (int k = rowIndex; k < rowIndex + ratio; k++) {
						if (asmModel.isRowDecoded(k)) {
							lineColor++;
						}
					}
					Color c = colorCache.get(lineColor);
					if (c == null) {
						int comp = Math.min(0xFF, (int) (lineColor / colorRatio));
						c = new Color(comp, 0x80, 0xFF - comp);
						colorCache.put(lineColor, c);
					}
					// FIXME: Use fillRect instead, and cache last color so we do fills per block, not per line
					g2.setColor(c);
					g2.drawLine(j, 0, j, clip.height - 1);

					if (posSize > 0) {
//						float alpha = 0.10f;
//						int type = AlphaComposite.SRC_OVER;
//						AlphaComposite composite = AlphaComposite.getInstance(type, alpha);
//						Composite origComposite = g2.getComposite();

						g2.setColor(COLOR_POS);
//						g2.setComposite(composite);
						g2.fillRect(pos, 0, posSize, clip.height);
//						g2.setComposite(origComposite);
					}
				}
			}

			g2.dispose();
		}

		@Override
		public void addNotify() {
			super.addNotify();
			Container parent = getParent();
			if (parent instanceof JViewport) {
				this.scrollPane = (JViewport) parent;
			}
		}

		@Override
		public void adjustmentValueChanged(AdjustmentEvent e) {
			Adjustable adjustable = e.getAdjustable();
			updatePosAndSize(adjustable);

			// FIXME: We should do dirty rects
			repaint();
		}

		private void updatePosAndSize(Adjustable adjustable) {
			double absPos = (double)adjustable.getValue() / adjustable.getMaximum();
			double absPosSize = (double)adjustable.getVisibleAmount() / adjustable.getMaximum();
			pos = (int) (absPos * getWidth());
			posSize = (int) Math.max(1, absPosSize * getWidth());
		}

		public void incZoom() {
			zoom++;
			if (normalWidth <= 0) {
				normalWidth = getWidth();
			}
			// FIXME: View should be centered around previous point after doLayout()
			setPreferredSize(new Dimension(normalWidth * zoom, getHeight()));
			scrollPane.doLayout();
			updatePosAndSize(asmDisplayVertScroll);
		}

		public void decZoom() {
			if (zoom > 1) {
				--zoom;
				if (normalWidth <= 0) {
					normalWidth = getWidth();
				}
				setPreferredSize(new Dimension(normalWidth * zoom, getHeight()));
				scrollPane.doLayout();
				updatePosAndSize(asmDisplayVertScroll);
			}
		}

		public void resetZoom() {
			zoom = 1;
			setPreferredSize(null);
			scrollPane.doLayout();
			normalWidth = getWidth();
		}
	}
}
