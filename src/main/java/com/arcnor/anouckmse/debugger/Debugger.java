package com.arcnor.anouckmse.debugger;

import com.arcnor.anouckmse.debugger.dialog.BreakpointsDialog;
import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog;
import com.arcnor.anouckmse.debugger.dialog.DebugDialog;
import com.arcnor.anouckmse.debugger.dialog.MemoryDialog;
import com.arcnor.anouckmse.swing.MutableListModel;
import com.arcnor.anouckmse.swing.Theming;
import com.arcnor.cpu.Flags;
import com.arcnor.cpu.Register;
import com.arcnor.cpu.debugger.CPUDebug;
import com.arcnor.system.CPUChip;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;
import javax.swing.border.BevelBorder;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class Debugger extends JFrame {
	private final CPUChip cpu;

	public static final Color COLOR_BACK_SELECTION = new Color(66, 68, 69);
	public static final Color COLOR_BACK_BREAKPOINT = new Color(135, 56, 45);
	public static final Color COLOR_BACK_CURRENT = new Color(49, 63, 127);
	public static final Color COLOR_BACK_GUTTER = new Color(49, 51, 53);
	public static final Color COLOR_FORE_REGCHANGED = Color.RED;

	private final Map<String, Action> onStepActions = new HashMap<String, Action>();
	private final BreakpointListModel<Integer> breakpointListModel = new BreakpointListModel<Integer>();

	private final JTable tblDisasm;
	private final Set<DebugDialog> debugDialogs = new LinkedHashSet<DebugDialog>();
	private final ASMTableModel asmTableModel;

	public static final String ACT_STEP_INTO = "stepInto";
	public static final String ACT_STEP_OVER = "stepOver";
	public static final String ACT_STEP_OUT  = "stepOut";
	public static final String ACT_CONTINUE  = "continue";

	private final AtomicBoolean RUNNING = new AtomicBoolean(false);

	private final Action actStepInto = new AbstractAction("Step Into") {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (!RUNNING.get()) {
				cpu.doCycle();
				onStepGraphical();
			}
			RUNNING.set(false);
		}
	};

	private final Action actStepOver = new AbstractAction("Step Over") {
		@Override
		public void actionPerformed(ActionEvent e) {
			// FIXME
			cpu.doCycle();
			RUNNING.set(false);
			onStepGraphical();
		}
	};

	private final Action actStepOut = new AbstractAction("Step Out") {
		@Override
		public void actionPerformed(ActionEvent e) {
			// FIXME
			cpu.doCycle();
			RUNNING.set(false);
			onStepGraphical();
		}
	};

	private final Action actContinue = new AbstractAction("Continue") {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (!RUNNING.get()) {
				RUNNING.set(true);
				SwingWorker sw = new SwingWorker() {
					@Override
					protected Object doInBackground() throws Exception {
						do {
							cpu.doCycle();
							onStep();
						} while (!breakpointListModel.contains(asmTableModel.addr2index(cpu.getPC())) && RUNNING.get());
						RUNNING.set(false);
						return null;
					}

					@Override
					protected void done() {
						onStepGraphical();      // A previous onStep() has been done before, no big deal
					}
				};
				sw.execute();
			}
		}
	};

	private final Action actLineToByte = new AbstractAction("Convert to byte") {
		@Override
		public void actionPerformed(final ActionEvent e) {
			//To change body of implemented methods use File | Settings | File Templates.
		}
	};

	private final Action actLineToWord = new AbstractAction("Convert to word") {
		@Override
		public void actionPerformed(final ActionEvent e) {
			//To change body of implemented methods use File | Settings | File Templates.
		}
	};

	private final Action actLineToString = new AbstractAction("Convert to string") {
		@Override
		public void actionPerformed(final ActionEvent e) {
			//To change body of implemented methods use File | Settings | File Templates.
		}
	};

	private final Set<KeyStroke> debugKeyStrokes = new LinkedHashSet<KeyStroke>();

	private final Action actUpdateView = new AbstractAction("Update View") {
		@Override
		public void actionPerformed(ActionEvent e) {
			// FIXME: We refresh the whole table right now, because we don't know what the old PC is (to repaint it as well)
			tblDisasm.repaint();
			gotoLine(asmTableModel.addr2index(cpu.getPC()));
		}
	};

	public <T extends CPUChip> Debugger(final T cpu, final CPUDebug<T> cpuDebug) {
		super("AnouckGGMS Debugger");
		this.cpu = cpu;

		setBackground(Theming.COLOR_BACK_WINDOW);
		setForeground(Theming.COLOR_FORE);

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());
		asmTableModel = new ASMTableModel<T>(cpu, cpuDebug, breakpointListModel);
		tblDisasm = new JTable(asmTableModel);
		tblDisasm.setIntercellSpacing(new Dimension(0, 0));
		tblDisasm.setForeground(getForeground());
		tblDisasm.setBackground(getBackground());
		tblDisasm.setSelectionBackground(COLOR_BACK_SELECTION);
		tblDisasm.setSelectionForeground(getForeground());
		tblDisasm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblDisasm.setShowGrid(false);
		tblDisasm.setTableHeader(null);
		final JPopupMenu popup = createDisasmPopup();
		//tblDisasm.setComponentPopupMenu();

		tblDisasm.setDefaultRenderer(Object.class, new SimpleRenderer());
		tblDisasm.setDefaultRenderer(Boolean.class, new BreakpointRenderer());

		tblDisasm.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JTable tbl = (JTable) e.getSource();
				int col = tbl.columnAtPoint(e.getPoint());
				if (col == 0) {
					int row = tbl.rowAtPoint(e.getPoint());
					Integer intObj = Integer.valueOf(row);
					if (breakpointListModel.contains(row)) {
						breakpointListModel.remove(intObj);
					} else {
						breakpointListModel.add(intObj);
					}
				}
			}

			public void mouseReleased(MouseEvent e)
			{
				if (e.isPopupTrigger() || e.getButton() == MouseEvent.BUTTON3) {
					JTable tbl = (JTable)e.getSource();
					int row = tbl.rowAtPoint( e.getPoint() );
					int column = tbl.columnAtPoint( e.getPoint() );

					if (!tbl.isRowSelected(row)) {
						tbl.changeSelection(row, column, false, false);
					}

					popup.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		onStepActions.put("UpdateView", actUpdateView);

		breakpointListModel.addListDataListener(new ListDataListener() {
			@Override
			public void intervalAdded(ListDataEvent e) {
				MutableListModel<Integer> model = (MutableListModel<Integer>) e.getSource();
				Integer row = (Integer) model.getElementAt(e.getIndex0());
				tblDisasm.setValueAt(Boolean.TRUE, row, 0);
			}

			@Override
			public void intervalRemoved(ListDataEvent e) {
				// FIXME: For the moment, we redraw the whole table as we don't know which index got removed
				tblDisasm.repaint();
			}

			@Override
			public void contentsChanged(ListDataEvent e) {
				//To change body of implemented methods use File | Settings | File Templates.
			}
		});

		TableColumnModel columnModel = tblDisasm.getColumnModel();
		columnModel.getColumn(ASMTableModel.COL_BREAKPOINT).setMaxWidth(20);
		columnModel.getColumn(ASMTableModel.COL_LABEL).setMaxWidth(60);
		columnModel.getColumn(ASMTableModel.COL_DATA).setMaxWidth(60);
		columnModel.getColumn(ASMTableModel.COL_OPCODE).setMaxWidth(260);
		JScrollPane scrollPane = new JScrollPane(tblDisasm);
		VisualRegionScroller disasmScrollBar = new VisualRegionScroller(JScrollBar.HORIZONTAL, asmTableModel, scrollPane.getVerticalScrollBar());
		add(disasmScrollBar, BorderLayout.NORTH);
		add(scrollPane, BorderLayout.CENTER);
		JPanel pnlInput = new JPanel();
		pnlInput.setBackground(Theming.COLOR_BACK_MENU);
		pnlInput.setLayout(new FlowLayout());
		for (int j = 0; j < cpu.getNumRegisters(); j++) {
			Register reg = cpu.getRegister(j);
			pnlInput.add(createRegLabel(reg));
		}
		for (int j = 0; j < cpu.getNumCounters(); j++) {
			pnlInput.add(createIntRegLabel(j));
		}
		pnlInput.add(createFlagsLabel(cpu.getFlags()));
		add(pnlInput, BorderLayout.SOUTH);
		setSize(800, 600);

		setLocationRelativeTo(null);

		disasmScrollBar.setBorder(Theming.BORDER_TOP);
		scrollPane.setBorder(BorderFactory.createEmptyBorder());
		pnlInput.setBorder(Theming.BORDER_TOP);

		updateActionMap();

		createDialogs();
		createMenuBar();

		gotoLine(asmTableModel.addr2index((int) cpu.getPORAddr()));
	}

	private void createDialogs() {
		debugDialogs.add(new ConsoleDialog(this));
		debugDialogs.add(new MemoryDialog(this, cpu.getMemory()));
		debugDialogs.add(new BreakpointsDialog(this, breakpointListModel));

		for (DebugDialog dialog : debugDialogs) {
			dialog.setVisible(true);
		}
	}

	private JMenuItem createMenuItem(Action act, KeyStroke keyStroke) {
		JMenuItem result = new JMenuItem(act);
		result.setAccelerator(keyStroke);
		return result;
	}

	private JPopupMenu createDisasmPopup() {
		JPopupMenu result = new JPopupMenu();
		final KeyStroke[] keyStrokes = {KeyStroke.getKeyStroke('b'), KeyStroke.getKeyStroke('w'), KeyStroke.getKeyStroke('s')};
		final Action[] actions = {actLineToByte, actLineToWord, actLineToString};

		for (int j = 0; j < keyStrokes.length; j++) {
			result.add(createMenuItem(actions[j], keyStrokes[j]));
		}

		result.addPopupMenuListener(new PopupMenuListener() {
			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				JPopupMenu source = (JPopupMenu) e.getSource();
				int r = tblDisasm.getSelectedRow();
				if (r < 0) {
					for (Action action : actions) {
						action.setEnabled(false);
					}
				} else {
					for (Action action : actions) {
						action.setEnabled(true);
					}
					ASMLine asmLine = asmTableModel.getASMLine(r);
					if (asmLine.opcode < 0) {
						switch (asmLine.opcode) {
							case ASMTableModel.DATA_BYTE:
								actLineToByte.setEnabled(false);
								break;
							case ASMTableModel.DATA_WORD:
								actLineToWord.setEnabled(false);
								break;
							case ASMTableModel.DATA_STRING:
								actLineToString.setEnabled(false);
								break;
						}
					}
				}
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				//To change body of implemented methods use File | Settings | File Templates.
			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {
				//To change body of implemented methods use File | Settings | File Templates.
			}
		});
		return result;
	}

	private void createMenuBar() {
		final JMenuBar menubar = new JMenuBar();
		setJMenuBar(menubar);
		menubar.setBorder(BorderFactory.createEmptyBorder());

		JMenu mnuDebug = new JMenu("Debug");
		menubar.add(mnuDebug);
		mnuDebug.setMnemonic('D');

		JPanel contentPane = (JPanel) getContentPane();
		InputMap inputMap = contentPane.getInputMap();
		ActionMap actionMap = contentPane.getActionMap();
		for (KeyStroke keyStroke : debugKeyStrokes) {
			Object key = inputMap.get(keyStroke);
			Action act = actionMap.get(key);
			mnuDebug.add(createMenuItem(act, keyStroke));
		}

		JMenu mnuWindow = new JMenu("Window");
		menubar.add(mnuWindow);
		mnuWindow.setMnemonic('W');

		AbstractAction actShowDialog = new AbstractAction("Show Dialog") {
			@Override
			public void actionPerformed(ActionEvent e) {
				JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();
				DebugDialog dialog = (DebugDialog) menubar.getClientProperty(item);
				dialog.setVisible(!dialog.isVisible());
			}
		};
		int idx = 0;
		for (DebugDialog dialog : debugDialogs) {
			final JCheckBoxMenuItem item = new JCheckBoxMenuItem(actShowDialog);
			dialog.addComponentListener(new ComponentAdapter() {
				@Override
				public void componentShown(ComponentEvent e) {
					item.setSelected(true);
				}

				@Override
				public void componentHidden(ComponentEvent e) {
					item.setSelected(false);
				}
			});
			item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_0 + idx++, InputEvent.CTRL_MASK));
			item.setSelected(dialog.isVisible());
			menubar.putClientProperty(item, dialog);
			item.setText(dialog.getTitle());
			mnuWindow.add(item);
		}
	}

	private void updateActionMap() {
		JPanel contentPane = (JPanel) getContentPane();

		InputMap inputMap = contentPane.getInputMap();
		ActionMap actionMap = contentPane.getActionMap();

		KeyStroke[] keyStrokes = {
				KeyStroke.getKeyStroke("F8"), KeyStroke.getKeyStroke("F7"),
				KeyStroke.getKeyStroke(KeyEvent.VK_F8, InputEvent.SHIFT_MASK), KeyStroke.getKeyStroke("F9")
		};
		String[] keys = {ACT_STEP_OVER, ACT_STEP_INTO, ACT_STEP_OUT, ACT_CONTINUE};
		Action[] actions = {actStepOver, actStepInto, actStepOut, actContinue};

		for (int j = 0; j < keyStrokes.length; j++) {
			inputMap.put(keyStrokes[j], keys[j]);
			actionMap.put(keys[j], actions[j]);
			debugKeyStrokes.add(keyStrokes[j]);
		}
	}

	private interface Function<T> {
		public T execute();
	}

	private interface Function1Arg<T, V> {
		public T execute(V arg1);
	}

	private JPanel createFlagsLabel(final Flags f) {
		final JPanel result = new JPanel(new FlowLayout());
		result.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));

		final Action updateFlagsAction = new AbstractAction("Flags") {
			private boolean[] flags = new boolean[8];

			@Override
			public void actionPerformed(ActionEvent e) {
				for (int j = 7; j >= 0; j--) {
					JLabel lbl = (JLabel) result.getComponent(7 - j);
					boolean flagSet = f.get(j);
					if (flags[j] != flagSet) {
						if (e == null || !e.getActionCommand().equals("manual")) {
							flags[j] = flagSet;
						}

						lbl.setForeground(COLOR_FORE_REGCHANGED);
					} else {
						lbl.setForeground(getForeground());
					}
					String fName = lbl.getText();
					lbl.setText(flagSet ? fName.toUpperCase() : fName.toLowerCase());
				}
				// Update flags as byte
				JLabel lbl = (JLabel) result.getComponent(8);
				lbl.setText(String.format("(%1$02X)", f.get()));
			}
		};
		onStepActions.put("F", updateFlagsAction);

		MouseListener lblMouseListener = new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JLabel lbl = (JLabel) e.getSource();
				Integer flag = (Integer) lbl.getClientProperty("flag");
				boolean flagSet = f.get(flag);
				f.set(flag, !flagSet);
				updateFlagsAction.actionPerformed(new ActionEvent(this, -1, "manual"));
			}
		};
		for (int j = 7; j >= 0; j--) {
			JLabel lbl = new JLabel(f.getName(j));
			lbl.putClientProperty("flag", j);
			lbl.addMouseListener(lblMouseListener);
			result.add(lbl);
		}
		result.add(new JLabel());   // Flags as byte
		updateFlagsAction.actionPerformed(null);
		return result;
	}

	private JPanel createRegLabel(final Register reg) {
		final String lbl = reg.getName();
		JPanel result = createCpuLabel(lbl, 4, new Function<Integer>() {
			@Override
			public Integer execute() {
				return reg.get();
			}
		}, new Function1Arg<Boolean, Integer>() {
			@Override
			public Boolean execute(Integer arg1) {
				if (arg1 < 0 || arg1 > 0xFFFF) {
					return false;
				}
				reg.set(arg1);
				return true;
			}
		});
		final JLabel lblObj = (JLabel) result.getComponent(0);
		Action updateLabelAction = new AbstractAction() {
			private final JLabel _lblObj = lblObj;
			private final String _lbl = lbl;
			private final Register _reg = reg;
			private short oldValue = 0;

			@Override
			public void actionPerformed(ActionEvent e) {
				short value = (short)_reg.get();
				if (_lblObj.getText().isEmpty() || oldValue != value) {
					_lblObj.setText(_lbl + ": " + String.format("%1$04X", value));
				}
				_lblObj.setForeground(oldValue != value ? COLOR_FORE_REGCHANGED : getForeground());
				oldValue = value;
			}
		};
		onStepActions.put(lbl, updateLabelAction);
		updateLabelAction.actionPerformed(null);
		return result;
	}

	private JPanel createIntRegLabel(final int regIdx) {
		final String lbl = cpu.getCounterName(regIdx);
		final int regSize = cpu.getCounterSize(regIdx);
		JPanel result = createCpuLabel(lbl, regSize, new Function<Integer>() {
			@Override
			public Integer execute() {
				return cpu.getCounter(regIdx);
			}
		}, new Function1Arg<Boolean, Integer>() {
			@Override
			public Boolean execute(Integer arg1) {
				if (arg1 < 0 || arg1 > (0xFFFFFFFF) >>> ((8 - regSize) << 2)) {
					return false;
				}
				cpu.setCounter(regIdx, arg1);
				return true;
			}
		});
		final JLabel lblObj = (JLabel) result.getComponent(0);
		Action updateLabelAction = new AbstractAction() {
			private final JLabel _lblObj = lblObj;
			private final String _lbl = lbl;
			private final int _regIdx = regIdx;
			private final int _regSize = regSize;
			private short oldValue = 0;

			@Override
			public void actionPerformed(ActionEvent e) {
				short value = (short)cpu.getCounter(_regIdx);
				if (_lblObj.getText().isEmpty() || oldValue != value) {
					_lblObj.setText(_lbl + ": " + String.format("%1$0" + _regSize + "X", value));
				}
				_lblObj.setForeground(oldValue != value ? COLOR_FORE_REGCHANGED : getForeground());
				oldValue = value;
			}
		};
		onStepActions.put(lbl, updateLabelAction);
		updateLabelAction.actionPerformed(null);
		return result;
	}

	private JPanel createCpuLabel(final String lbl, final int regSize, final Function<Integer> defaultValueFunc, final Function1Arg<Boolean, Integer> setValueFunc) {
		JPanel result = new JPanel();
		result.setBackground(getBackground());
		JLabel lblObj = new JLabel();
		lblObj.setForeground(getForeground());
		result.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		result.add(lblObj);

		result.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Integer defaultValue = defaultValueFunc.execute();
				String defaultValueStr = String.format("%1$0" + regSize + "X", defaultValue);
				String inputResult = (String) JOptionPane.showInputDialog(Debugger.this, "Enter new value:", lbl + " Register", JOptionPane.QUESTION_MESSAGE, null, null, defaultValueStr);
				if (inputResult != null) {
					try {
						Integer intObj = Integer.decode("0x" + inputResult);
						Boolean setValueResult = setValueFunc.execute(intObj);
						if (!setValueResult) {
							JOptionPane.showMessageDialog(Debugger.this, "Invalid value (check bounds or value itself)", "Invalid value", JOptionPane.ERROR_MESSAGE);
						} else {
							onStepActions.get(lbl).actionPerformed(null);
						}
					} catch (NumberFormatException ex) {
						JOptionPane.showMessageDialog(Debugger.this, "New value must be a positive hexadecimal number", "Invalid value", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		return result;
	}

	private void onStep() {
		int pc = cpu.getPC();
		if (!asmTableModel.isRowDecoded(asmTableModel.addr2index(pc))) {
			asmTableModel.decodeAddress(pc);
		}
		for (DebugDialog dialog : debugDialogs) {
			dialog.onStep();
		}
	}

	private void onStepGraphical() {
		onStep();
		for (DebugDialog dialog : debugDialogs) {
			dialog.onGraphicalStep();
		}
		for (Action act : onStepActions.values()) {
			act.actionPerformed(null);
		}
	}

	public void gotoLine(int idx) {
		tblDisasm.setRowSelectionInterval(idx, idx);
		Rectangle cellRect = new Rectangle(tblDisasm.getCellRect(idx, 0, true));
		Rectangle visRect = tblDisasm.getVisibleRect();
		cellRect.setSize(visRect.getSize());
		cellRect.translate(0, -visRect.height / 2);
		tblDisasm.scrollRectToVisible(cellRect);
	}

	public CPUChip getCPU() {
		return cpu;
	}

	public ASMLine getDecodedLine(int addr) {
		return asmTableModel.getASMLine(asmTableModel.addr2index(addr));
	}


	private class SimpleRenderer extends DefaultTableCellRenderer {
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			Component result = super.getTableCellRendererComponent(table, value, isSelected, false, row, column);
			int pc = cpu.getPC();
			if (asmTableModel.addr2index(pc) == row) {
				super.setBackground(COLOR_BACK_CURRENT);
			} else if (breakpointListModel.contains(row)) {
				super.setBackground(COLOR_BACK_BREAKPOINT);
			} else {
				super.setBackground(isSelected ? table.getSelectionBackground() : table.getBackground());
			}
			return result;
		}
	}

	private class BreakpointRenderer implements TableCellRenderer {
		private final JLabel painter = new JLabel();
		private final Icon ICON_BREAKPOINT;

		public static final String PROP_BREAKPOINT = "breakpoint";

		private BreakpointRenderer() {
			painter.setOpaque(true);
			painter.setBackground(COLOR_BACK_GUTTER);
			ICON_BREAKPOINT = new ImageIcon(this.getClass().getResource("/breakpoint.png"));
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			painter.setIcon((Boolean)value ? ICON_BREAKPOINT : null);
			painter.setBackground(isSelected ? table.getSelectionBackground() : COLOR_BACK_GUTTER);
			return painter;
		}
	}
}
