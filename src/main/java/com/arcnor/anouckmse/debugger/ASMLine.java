package com.arcnor.anouckmse.debugger;

public class ASMLine {
	public int addr;
	public int opcode;
	public int data;
	public final byte[] buffer;
	public String decoded;
	public String label;

	public ASMLine(int addr, int opcode, int data, byte[] buffer, String decoded) {
		this.addr = addr;
		this.opcode = opcode;
		this.data = data;
		this.buffer = buffer;
		this.decoded = decoded;
	}
}
