package com.arcnor.anouckmse.debugger.dialog;

import com.arcnor.anouckmse.debugger.Debugger;
import com.arcnor.anouckmse.debugger.command.ClockCommand;
import com.arcnor.anouckmse.debugger.command.Command;
import com.arcnor.anouckmse.debugger.command.ContinueCommand;
import com.arcnor.anouckmse.debugger.command.HelpCommand;
import com.arcnor.anouckmse.debugger.command.LogCommand;
import com.arcnor.anouckmse.debugger.command.PrintCommand;
import com.arcnor.anouckmse.debugger.command.StepIntoCommand;
import com.arcnor.anouckmse.debugger.command.StepOutCommand;
import com.arcnor.anouckmse.debugger.command.StepOverCommand;
import com.arcnor.anouckmse.debugger.command.WatchCommand;
import com.arcnor.anouckmse.swing.Theming;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Document;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ConsoleDialog extends DebugDialog {
	private final Debugger debugger;

	private final JTextPane txtConsole;
	private final Action actCmd;

	public static final String STYLE_USER  = "user";
	public static final String STYLE_ERROR = "error";
	public static final String STYLE_INFO  = "info";
	public static final String STYLE_DATA  = "data";

	private static final Color COLOR_FORE_USER  = Color.WHITE;
	private static final Color COLOR_FORE_ERROR = Color.RED;
	private static final Color COLOR_FORE_INFO = new Color(99, 169, 104);
	private static final Color COLOR_FORE_DATA = Color.YELLOW;

	private Map<String, Command> commands = new TreeMap<String, Command>();
	private List<Command> stepCommands = new ArrayList<Command>();

	public ConsoleDialog(Debugger owner) {
		super(owner, "Console", false);

		this.debugger = owner;

		setLayout(new BorderLayout());
		txtConsole = new JTextPane();
		txtConsole.setBackground(owner.getBackground());
		txtConsole.setForeground(Theming.COLOR_FORE);
		txtConsole.setEditable(false);
		txtConsole.setMargin(new Insets(4, 4, 4, 4));

		DefaultCaret caret = (DefaultCaret) txtConsole.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		createStyles(txtConsole);
		add(new JScrollPane(txtConsole), BorderLayout.CENTER);

		JPanel pnlInput = new JPanel(new BorderLayout());
		pnlInput.setBorder(Theming.BORDER_TOP);
		pnlInput.setBackground(Theming.COLOR_BACK_MENU);

		add(pnlInput, BorderLayout.SOUTH);

		fillCommandMap();
		actCmd = createCommandAction();
		final JTextField txtCmdInput = new JTextField();
		txtCmdInput.setAction(actCmd);
		JPanel pnlCmdInputContainer = new JPanel(new BorderLayout());
		pnlCmdInputContainer.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		pnlCmdInputContainer.add(txtCmdInput);
		pnlInput.add(pnlCmdInputContainer, BorderLayout.CENTER);

		setSize(450, owner.getHeight() / 2);
		Point ownerLoc = owner.getLocation();
		setLocation((int) (ownerLoc.getX() - getBounds().getWidth()), (int) ownerLoc.getY());

		append("Console ready for action\nType ? to get a list of commands\n\n");
		this.addWindowFocusListener(new WindowAdapter() {
			@Override
			public void windowGainedFocus(WindowEvent e) {
				txtCmdInput.requestFocusInWindow();
			}
		});
	}

	private void fillCommandMap() {
		Class[] clazzez = new Class[]{
				HelpCommand.class, StepIntoCommand.class, StepOverCommand.class, StepOutCommand.class,
				ContinueCommand.class, ClockCommand.class, PrintCommand.class, LogCommand.class,
				WatchCommand.class
		};

		for (Class clazz : clazzez) {
			try {
				Constructor<Command> constructor = clazz.getConstructor(ConsoleDialog.class);
				Command cmd = constructor.newInstance(this);
				commands.put(cmd.getName(), cmd);
				if (cmd.isStepCommand()) {
					stepCommands.add(cmd);
				}
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

	private void createStyles(JTextPane txtPane) {
		Style stlUser = txtPane.addStyle(STYLE_USER, null);
		StyleConstants.setForeground(stlUser, COLOR_FORE_USER);

		Style stlError = txtPane.addStyle(STYLE_ERROR, null);
		StyleConstants.setForeground(stlError, COLOR_FORE_ERROR);

		Style stlInfo = txtPane.addStyle(STYLE_INFO, null);
		StyleConstants.setForeground(stlInfo, COLOR_FORE_INFO);

		Style stlData = txtPane.addStyle(STYLE_DATA, null);
		StyleConstants.setForeground(stlData, COLOR_FORE_DATA);
	}

	private Action createCommandAction() {
		final AbstractAction result = new AbstractAction("Process Command") {
			@Override
			public void actionPerformed(ActionEvent e) {
				JTextField txt = (JTextField) e.getSource();
				String cmdStr = txt.getText().trim();
				if (cmdStr.isEmpty()) {
					return;
				}
				txt.setText("");
				append("> " + cmdStr + "\n", STYLE_USER);
				String cmd0 = cmdStr.split("\\s+")[0];
				Command cmd = commands.get(cmd0.toLowerCase());
				if (cmd == null) {
					append("Unknown command\n", STYLE_ERROR);
					return;
				}
				cmd.getAction().actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, cmdStr));
			}
		};

		return result;
	}

	public void append(String line) {
		append(line, null);
	}

	public void append(String line, String style) {
		Document doc = txtConsole.getDocument();
		Style stl = style != null
			? txtConsole.getStyle(style)
			: null;
		try {
			doc.insertString(doc.getLength(), line, stl);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	public Iterator<Command> getCommands() {
		return commands.values().iterator();
	}

	public Debugger getDebugger() {
		return debugger;
	}

	@Override
	public void onStep() {
		for (int j = 0; j < stepCommands.size(); j++) {
			stepCommands.get(j).onStep();
		}
	}

	@Override
	public void onGraphicalStep() {
		// Do nothing, yet...
	}
}
