package com.arcnor.anouckmse.debugger.dialog;

import com.arcnor.anouckmse.debugger.Debugger;
import com.arcnor.anouckmse.swing.Theming;
import com.arcnor.system.MemoryChip;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JPanel;
import tv.porst.jhexview.AbstractDataProvider;
import tv.porst.jhexview.JHexView;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;

public class MemoryDialog extends DebugDialog {
	private final MemoryDataModel memoryDataModel;

	public MemoryDialog(Debugger owner, MemoryChip mem) {
		super(owner, "Memory", false);

		setLayout(new BorderLayout());

		final JHexView hexView = new JHexView();
		hexView.setBackgroundColorAsciiView(Theming.COLOR_BACK_WINDOW);
		hexView.setBackgroundColorHexView(Theming.COLOR_BACK_WINDOW);
		hexView.setFontColorAsciiView(Theming.COLOR_STRING);
		hexView.setFontColorHexView1(Theming.COLOR_RESERVED);
		hexView.setFontColorHexView2(Theming.COLOR_RESERVED.brighter());
		hexView.setLineColorDivider(Theming.COLOR_LINE);
		hexView.setSelectionColor(Theming.COLOR_SELECTION);
		hexView.setBackgroundColorOffsetView(Theming.COLOR_BACK_MENU);
		hexView.setFontColorOffsetView(Theming.COLOR_FORE);

		memoryDataModel = new MemoryDataModel(mem);
		hexView.setData(memoryDataModel);
		hexView.setDefinitionStatus(JHexView.DefinitionStatus.DEFINED);
		hexView.setEnabled(true);
		add(hexView, BorderLayout.CENTER);
		JPanel pnlButtons = new JPanel();
		pnlButtons.setLayout(new FlowLayout());
		add(pnlButtons, BorderLayout.SOUTH);

		String[] regionNames = mem.getRegionNames();
		for (int j = 0; j < regionNames.length; j++) {
			String region = regionNames[j];
			final int regionIdx = j;
			pnlButtons.add(new JButton(new AbstractAction(region) {
				@Override
				public void actionPerformed(ActionEvent e) {
					memoryDataModel.setRegion(regionIdx);
					hexView.setBaseAddress(memoryDataModel.getRegionOffset());
				}
			}));
		}

		setSize(450, owner.getHeight() / 2);
		Point ownerLoc = owner.getLocation();
		setLocation((int) (ownerLoc.getX() - getBounds().getWidth()), (int) ownerLoc.getY() + getHeight());
	}

	@Override
	public void onStep() {
		// Do nothing, because we don't care that the memory dialog is out-of-sync during long runs
	}

	@Override
	public void onGraphicalStep() {
		memoryDataModel.onStep();
	}

	private class MemoryDataModel extends AbstractDataProvider {
		private final MemoryChip mem;

		private int region;

		public MemoryDataModel(MemoryChip mem) {
			this.mem = mem;
		}

		public void onStep() {
			notifyListeners();
		}

		public int getRegion() {
			return region;
		}

		public void setRegion(int idx) {
			this.region = idx;

			notifyListeners();
		}

		public long getRegionOffset() {
			return mem.getRegionOffset(region, 0);
		}

		@Override
		public byte[] getData(long offset, long length) {
			return mem.getData((int) offset, (int) length);
		}

		@Override
		public long getDataLength() {
			return mem.getRegionLength(region);
		}

		@Override
		public void setData(final long addr, final byte[] bytes) {
			for (int j = 0; j < bytes.length; j++) {
				mem.write((int) (addr + j), bytes[j]);
			}
			notifyListeners();
		}

		@Override
		public boolean hasData(long offset, long length) {
			return true;
		}

		@Override
		public boolean isEditable() {
			return true;
		}

		@Override
		public boolean keepTrying() {
			return false;
		}
	}
}
