package com.arcnor.anouckmse.debugger.dialog;

import com.arcnor.anouckmse.debugger.Debugger;
import com.arcnor.anouckmse.swing.MutableListModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class BreakpointsDialog extends DebugDialog {
	private final Action actDelete;
	private final Action actGoLine;
	private final MutableListModel<Integer> breakpointListModel;

	public BreakpointsDialog(final Debugger owner, final MutableListModel<Integer> breakpointListModel) {
		super(owner, "Breakpoint list", false);

		this.breakpointListModel = breakpointListModel;
		actDelete = new AbstractAction("Delete breakpoint") {
			@Override
			public void actionPerformed(ActionEvent e) {
				JList list = (JList) e.getSource();
				int selIdx = list.getSelectedIndex();
				if (selIdx >= 0) {
					BreakpointsDialog.this.breakpointListModel.remove(selIdx);
				}
			}
		};
		actGoLine = new AbstractAction("Goto breakpoint line") {
			@Override
			public void actionPerformed(ActionEvent e) {
				JList list = (JList) e.getSource();
				int selIdx = list.getSelectedIndex();
				if (selIdx >= 0) {
					Integer bpLine = (Integer) BreakpointsDialog.this.breakpointListModel.getElementAt(selIdx);
					owner.gotoLine(bpLine);
				}
			}
		};

		setLayout(new BorderLayout());
		JList list = new JList(breakpointListModel);
		list.setBackground(owner.getBackground());
		list.setForeground(owner.getForeground());
		list.setSelectionBackground(Debugger.COLOR_BACK_SELECTION);
		list.setSelectionForeground(owner.getForeground());
		list.getInputMap().put(KeyStroke.getKeyStroke("DELETE"), "delete");
		list.getActionMap().put("delete", actDelete);
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					int idx = ((JList)e.getSource()).locationToIndex(e.getPoint());
					if (idx >= 0) {
						actGoLine.actionPerformed(new ActionEvent(e.getSource(), ActionEvent.ACTION_PERFORMED, "dblClick"));
					}
				}
			}
		});
		add(new JScrollPane(list));

		setSize(100, owner.getHeight());
		Point ownerLoc = owner.getLocation();
		setLocation((int)ownerLoc.getX() + owner.getWidth(), (int) ownerLoc.getY());
	}

	@Override
	public void onStep() {
		// Do nothing
	}

	@Override
	public void onGraphicalStep() {
		// Do nothing
	}
}
