package com.arcnor.anouckmse.debugger.dialog;

import com.arcnor.anouckmse.debugger.Debugger;
import javax.swing.JDialog;

import java.awt.GraphicsConfiguration;

public abstract class DebugDialog extends JDialog {
	protected DebugDialog(Debugger owner, String title, boolean modal) {
		super(owner, title, modal);
	}

	protected DebugDialog(Debugger owner, String title, boolean modal, GraphicsConfiguration gc) {
		super(owner, title, modal, gc);
	}

	public abstract void onStep();
	public abstract void onGraphicalStep();
}
