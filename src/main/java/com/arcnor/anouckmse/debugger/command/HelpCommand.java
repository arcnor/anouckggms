package com.arcnor.anouckmse.debugger.command;

import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Iterator;

public class HelpCommand extends Command {
	public HelpCommand(final ConsoleDialog console) {
		super(console, "?", "Provides a list of commands", new AbstractAction("Help Command") {
			@Override
			public void actionPerformed(ActionEvent e) {
				console.append("Currently known commands are:\n", ConsoleDialog.STYLE_INFO);
				Iterator<Command> it = console.getCommands();
				while (it.hasNext()) {
					Command cmd = it.next();
					console.append("   " + cmd.getName() + "\t" + cmd.getDesc() + "\n", ConsoleDialog.STYLE_INFO);
				}
			}
		});
	}
}
