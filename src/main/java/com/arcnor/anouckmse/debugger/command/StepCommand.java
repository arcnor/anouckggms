package com.arcnor.anouckmse.debugger.command;

import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog;

import javax.swing.*;

public abstract class StepCommand extends Command {
	public StepCommand(ConsoleDialog console, String name, String desc, String actionKey) {
		super(console, name, desc, ((JPanel)console.getDebugger().getContentPane()).getActionMap().get(actionKey));
	}
}
