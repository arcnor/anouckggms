package com.arcnor.anouckmse.debugger.command;

import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog;
import com.arcnor.system.CPUChip;
import com.arcnor.system.MemoryChip;
import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;

public class WatchCommand extends Command {
	public static final Map<Integer, Integer> addrs = new HashMap<Integer, Integer>();

	public WatchCommand(final ConsoleDialog console) {
		super(console, "w", "Watch an address for a change in value", new AbstractAction("Watch Command") {
			@Override
			public void actionPerformed(ActionEvent e) {
				String cmd = e.getActionCommand();
				String[] args = cmd.split("\\s+");
				if (args.length != 2) {
					console.append("Watch command takes 1 parameter\n", ConsoleDialog.STYLE_ERROR);
					return;
				}
				try {
					int addr = Integer.valueOf(args[1], 16);
					final MemoryChip mem = console.getDebugger().getCPU().getMemory();
					int value = mem.readUnsigned(addr);
					addrs.put(addr, value);
					console.append(String.format("Added watch at %1$04X (current value %2$02X)\n", addr, value), ConsoleDialog.STYLE_INFO);
				} catch (Exception ex) {
					console.append("Watch command parameter must be a valid hexadecimal address (i.e. 007F)\n", ConsoleDialog.STYLE_ERROR);
				}
			}
		});
	}

	@Override
	public boolean isStepCommand() {
		return true;
	}

	@Override
	public void onStep() {
		final CPUChip cpu = console.getDebugger().getCPU();
		final int pc = cpu.getPC();
		final MemoryChip mem = cpu.getMemory();
		for (Map.Entry<Integer, Integer> entry : addrs.entrySet()) {
			final Integer addr = entry.getKey();
			Integer old = entry.getValue();
			int curr = mem.readUnsigned(addr);
			if (curr != old) {
				console.append(String.format("%1$04X - Watch[%2$04X]: %3$02X -> %4$02X\n", pc, addr, old, curr), ConsoleDialog.STYLE_DATA);
				addrs.put(addr, curr);
			}
		}
	}
}
