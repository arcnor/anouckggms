package com.arcnor.anouckmse.debugger.command;

import com.arcnor.anouckmse.debugger.Debugger;
import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog;

public class StepOutCommand extends StepCommand {
	public StepOutCommand(ConsoleDialog console) {
		super(console, "so", "Step out", Debugger.ACT_STEP_OUT);
	}
}
