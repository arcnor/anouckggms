package com.arcnor.anouckmse.debugger.command;

import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog;

import javax.swing.*;

public abstract class Command {
	protected final String name, desc;
	protected final Action action;
	protected final ConsoleDialog console;

	public Command(ConsoleDialog console, String name, String desc, Action action) {
		this.console = console;
		this.name = name;
		this.desc = desc;
		this.action = action;
	}

	public Action getAction() {
		return action;
	}

	public String getDesc() {
		return desc;
	}

	public String getName() {
		return name;
	}

	public boolean isStepCommand() {
		return false;
	}

	public void onStep() {
		// Do nothing
	}
}
