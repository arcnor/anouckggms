package com.arcnor.anouckmse.debugger.command;

import com.arcnor.anouckmse.debugger.ASMLine;
import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog;
import com.arcnor.cpu.Register;
import com.arcnor.system.CPUChip;
import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;

public class LogCommand extends Command {
	private static boolean logEnabled = false;

	private final StringBuilder sb = new StringBuilder();

	public LogCommand(final ConsoleDialog console) {
		super(console, "log", "Logs status of the CPU after every step", new AbstractAction("Log Command") {
			@Override
			public void actionPerformed(ActionEvent e) {
				logEnabled = !logEnabled;
				console.append("Logging is now " + (logEnabled ? "enabled" : "disabled") + '\n');
			}
		});
	}

	@Override
	public boolean isStepCommand() {
		return true;
	}

	@Override
	public void onStep() {
		if (logEnabled) {
			final CPUChip cpu = console.getDebugger().getCPU();

			sb.setLength(0);

			ASMLine line = console.getDebugger().getDecodedLine(cpu.getPC());

			for (byte b : line.buffer) {
				sb.append(String.format("%1$02X ", b));
			}
			sb.setLength(sb.length() - 1);  // Trim last space
			sb.append('\t');
			// FIXME: ".decoded" is not right because it has the comment attached. Split that
//			sb.append('\t').append(line.decoded);

			int nRegs = cpu.getNumRegisters();
			for (int j = 0; j < nRegs; j++) {
				Register reg = cpu.getRegister(j);
				sb.append(reg.getName()).append(':').append(String.format("%1$04X", reg.get())).append(' ');
			}

			int nCounters = cpu.getNumCounters();
			for (int j = 0; j < nCounters; j++) {
				int value = cpu.getCounter(j);
				sb.append(cpu.getCounterName(j)).append(':').append(String.format("%1$0" + cpu.getCounterSize(j) + "X", value)).append(' ');
			}

			sb.append("F:").append(String.format("%1$02X", cpu.getFlags().get())).append(' ');
			sb.append("Cycle:").append(cpu.getClockCycles());

			console.append(String.format("%1$04X\t%2$s\n", cpu.getPC(), sb.toString()), ConsoleDialog.STYLE_DATA);
		}
	}
}
