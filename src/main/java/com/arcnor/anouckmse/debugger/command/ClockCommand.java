package com.arcnor.anouckmse.debugger.command;

import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog;
import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;

public class ClockCommand extends Command {
	public ClockCommand(final ConsoleDialog console) {
		super(console, "clock", "Show CPU clock cycles", new AbstractAction("Clock Command") {
			@Override
			public void actionPerformed(ActionEvent e) {
				long cycles = console.getDebugger().getCPU().getClockCycles();
				console.append("Clock: " + cycles + " cycles\n");
			}
		});
	}
}
