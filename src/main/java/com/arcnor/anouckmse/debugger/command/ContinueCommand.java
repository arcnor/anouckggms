package com.arcnor.anouckmse.debugger.command;

import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class ContinueCommand extends Command {
	public ContinueCommand(ConsoleDialog console) {
		super(console, "c", "Continue execution", new AbstractAction("Continue Command") {
			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
	}
}
