package com.arcnor.anouckmse.debugger.command;

import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class PrintCommand extends Command {
	public PrintCommand(ConsoleDialog console) {
		super(console, "p", "Prints register or memory", new AbstractAction("Print Command") {
			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
	}
}
