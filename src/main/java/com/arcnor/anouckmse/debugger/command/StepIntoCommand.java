package com.arcnor.anouckmse.debugger.command;

import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog;
import com.arcnor.anouckmse.debugger.Debugger;

public class StepIntoCommand extends StepCommand {
	public StepIntoCommand(ConsoleDialog console) {
		super(console, "si", "Step into", Debugger.ACT_STEP_INTO);
	}
}
