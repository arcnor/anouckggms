package com.arcnor.anouckmse.debugger.command;

import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog;
import com.arcnor.anouckmse.debugger.Debugger;

public class StepOverCommand extends StepCommand {
	public StepOverCommand(ConsoleDialog console) {
		super(console, "s", "Step over", Debugger.ACT_STEP_OVER);
	}
}
