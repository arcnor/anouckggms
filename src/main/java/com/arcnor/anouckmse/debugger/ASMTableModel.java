package com.arcnor.anouckmse.debugger;

import com.arcnor.cpu.debugger.CPUDebug;
import com.arcnor.cpu.debugger.JumpType;
import com.arcnor.cpu.debugger.OpcodeInfo;
import com.arcnor.system.CPUChip;
import com.arcnor.system.MemoryChip;
import javax.swing.table.AbstractTableModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

class ASMTableModel<T extends CPUChip> extends AbstractTableModel {
	private final MemoryChip mem;
	private final CPUDebug<T> cpuDebug;
	private final BreakpointListModel<Integer> breakpointListModel;

	private final List<ASMLine> disasm = new ArrayList<ASMLine>();
	private final Map<Integer, Integer> addr2list = new HashMap<Integer, Integer>();

	// FIXME
	private final ASMLine[] cells = new ASMLine[0x10000];

	public static final int COL_BREAKPOINT = 0;
	public static final int COL_LABEL      = 1;
	public static final int COL_DATA       = 2;
	public static final int COL_OPCODE     = 3;
	public static final int COL_COMMENT    = 4;

	public static final int DATA_BYTE = -1;
	public static final int DATA_WORD = -2;
	public static final int DATA_STRING = -3;

	public ASMTableModel(T cpu, CPUDebug<T> cpuDebug, BreakpointListModel<Integer> breakpointListModel) {
		mem = cpu.getMemory();
		this.cpuDebug = cpuDebug;
		this.breakpointListModel = breakpointListModel;

		final TreeSet<Integer> addrVisit = new TreeSet<Integer>();

		int porAddr = (int) cpu.getPORAddr();
		addrVisit.add(porAddr);
		addrVisit.add((int)cpu.getNMIAddr());
		for (long addr : cpu.getINTAddrs()) {
			addrVisit.add((int)addr);
		}

		decodeAddresses(addrVisit);
	}

	private void decodeAddresses(TreeSet<Integer> addrVisit) {
		final TreeSet<Integer> addrAll = new TreeSet<Integer>();

		while (!addrVisit.isEmpty()) {
			addrAll.addAll(addrVisit);
			decodeUntilRet(addrVisit.pollFirst(), addrVisit);
		}
		ASMLine oldLine = null;
		addr2list.clear();
		disasm.clear();
		for (int j = 0; j < cells.length; j++) {
			ASMLine line = cells[j];
			if (line == null) {
				byte data = mem.readSigned(j);
				String binStr = String.format("%1$8s", Integer.toBinaryString(data & 0xFF)).replace(' ', '0');
				line = new ASMLine(j, DATA_BYTE, data, new byte[]{data}, String.format("DB %1$02Xh [%2$s] (%3$s)", data, binStr, (char)data));
				cells[j] = line;
			}
			if (line != oldLine) {
				addr2list.put(j, disasm.size());
				disasm.add(line);
				oldLine = line;

				// Add labels
				if (addrAll.contains(j)) {
					line.label = cpuDebug.formatLabel(j);
				}
			}
		}
		fireTableDataChanged();
	}

	private void decodeUntilRet(int addr, TreeSet<Integer> addrVisit) {
		if (cells[addr] != null && cells[addr].opcode >= 0) {
			return;
		}
		OpcodeInfo opcodeInfo;
		while (true) {
			int oldAddr = addr;
			int opcode = mem.readUnsigned(addr++);

			opcodeInfo = cpuDebug.decodeOpcodeInfo(opcode);

			byte[] buf = new byte[opcodeInfo.opcodeSize + opcodeInfo.dataSize];
			buf[0] = (byte)opcode;

			int idx = opcodeInfo.opcodeSize;
			while (idx > 1) {
				opcode <<= 8;
				byte b = mem.readSigned(addr++);
				buf[addr - oldAddr - 1] = b;
				opcode |= b;
				idx--;
			}

			int data = 0;
			idx = opcodeInfo.dataSize;
			while (idx > 0) {
				byte b = mem.readSigned(addr++);
				data |= (b & 0xFF) << (opcodeInfo.dataSize - idx) * 8;
				buf[addr - oldAddr - 1] = b;
				idx--;
			}

			ASMLine line = new ASMLine(oldAddr, opcode, data, buf, cpuDebug.opToStr(oldAddr, buf[0] & 0xFF, opcodeInfo.opcodeSize > 1 ? buf[1] & 0xFF : 0, data, null));
			while (oldAddr < addr) {
				cells[oldAddr++] = line;
			}

			// Check if opcode is a jump
			JumpType jt = cpuDebug.opcodeJumpType(opcode);
			if (jt != JumpType.NO_JUMP) {
				switch (jt) {
					case RET:
					case HALT:
						return;
					case JUMP_UNKNOWN:
					case JUMP_REL_UNKNOWN:
						// Don't know the jump, but we have to return
						return;
					case CALL_UNKNOWN:
					case RET_COND:
						// Do nothing for now
						break;
					case JUMP: addrVisit.add(data); return;
					case JUMP_REL: addrVisit.add(addr + (byte)data); return;
					case JUMP_REL_COND: addrVisit.add(addr + (byte)data); break;
					case CALL:
					case CALL_COND:
					case JUMP_COND:
						addrVisit.add(data);
						break;
				}
			}
		}
	}

	@Override
	public int getRowCount() {
		return disasm.size();
	}

	@Override
	public int getColumnCount() {
		return 5;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return columnIndex == 0 ? Boolean.class : String.class;
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (columnIndex == 0) {
			fireTableRowsUpdated(rowIndex, rowIndex);
		}
	}

	private final StringBuilder sb = new StringBuilder();

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		ASMLine line = disasm.get(rowIndex);
		switch (columnIndex) {
			case COL_BREAKPOINT: return breakpointListModel.contains(rowIndex);
			case COL_LABEL: return line.label;
			case COL_DATA: return String.format("0x%1$04X", line.addr);
			case COL_OPCODE:
				sb.setLength(0);
				for (byte b : line.buffer) {
					sb.append(String.format("%1$02X ", b));
				}
				return sb.toString();
			case COL_COMMENT: return line.decoded;
		}
		return null;
	}

	public boolean isRowDecoded(int rowIndex) {
		ASMLine asmLine = disasm.get(rowIndex);
		return asmLine != null && asmLine.opcode >= 0 && asmLine.buffer[0] == mem.readSigned(asmLine.addr);
	}

	public int addr2index(int addr) {
		return addr2list.get(addr);
	}

	public ASMLine getASMLine(int rowIndex) {
		return disasm.get(rowIndex);
	}

	public void decodeAddress(int addr) {
		TreeSet<Integer> addrVisit = new TreeSet<Integer>();
		addrVisit.add(addr);
		// Force addr to be decoded, remove it from cells
		cells[addr] = null;
		decodeAddresses(addrVisit);
	}
}
