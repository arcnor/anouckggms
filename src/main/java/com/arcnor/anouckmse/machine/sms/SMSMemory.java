package com.arcnor.anouckmse.machine.sms;

import com.arcnor.system.MemoryChip;

import java.util.Arrays;

public class SMSMemory implements MemoryChip {
	// Memory Frames (4x16K)
	private final byte[][] frames;
	// ROM pages (number depending on rom size)
	private final byte[][] pages;
	// Cartridge RAM pages (2x16K)
	private final byte[][] cartRAMPages;

	public SMSMemory(byte[] mem) {
		frames = new byte[4][0x4000];
		pages = new byte[Math.max(mem.length / 0x4000, 2)][0x4000];
		cartRAMPages = new byte[2][0x4000];

		// Default mapping
		frames[0] = pages[0];
		frames[1] = pages[1];

		// Copy RAM
		int nPages = mem.length / 0x4000;
		for (int n = 0; n < nPages; n++) {
			System.arraycopy(mem, n * 0x4000, pages[n], 0, 0x4000);
		}
	}

	@Override
	public byte readSigned(int addr) {
		if (addr < 0x400) {
			return pages[0][addr];  // First 1K, not paged
		} else if (addr < 0x4000) {
			return frames[0][addr]; // ROM page 0 (15K)
		} else if (addr < 0x8000) {
			return frames[1][addr - 0x4000];    // ROM page 1 (16K)
		} else if (addr < 0xC000) {
			return frames[2][addr - 0x8000];    // ROM page 2 or Cart RAM (16K)
		} else if (addr < 0xE000) {
			return frames[3][addr - 0xA000];    // RAM (8K)
		} else if (addr < 0x10000) {
			return frames[3][addr - 0xC000];    // Mirrored RAM (8K)
		} else {
			return 0;                           // Out of bounds
		}
	}

	@Override
	public int readUnsigned(int addr) {
		return readSigned(addr) & 0xFF;
	}

	@Override
	public void write(int addr, byte value) {
		if (addr < 0x400) {
			pages[0][addr] = value;  // First 1K, not paged
		} else if (addr < 0x4000) {
			frames[0][addr] = value; // ROM page 0 (15K)
		} else if (addr < 0x8000) {
			frames[1][addr - 0x4000] = value;    // ROM page 1 (16K)
		} else if (addr < 0xC000) {
			frames[2][addr - 0x8000] = value;    // ROM page 2 or Cart RAM (16K)
		} else if (addr < 0xE000) {
			frames[3][addr - 0xA000] = value;    // RAM (8K)
		} else if (addr < 0x10000) {
			frames[3][addr - 0xC000] = value;    // Mirrored RAM (8K)
		} else {
			// Out of bounds
		}
	}

	/**
	 * This will ONLY return continuous memory, and will NOT perform any boundary copy magic
	 */
	@Override
	public byte[] getData(int addr, int length) {
		byte[] src;
		int realAddr = 0;
		if (addr < 0x400) {
			src = pages[0];
			realAddr = addr;
		} else if (addr < 0x4000) {
			src = frames[0];
			realAddr = addr;
		} else if (addr < 0x8000) {
			src = frames[1];
			realAddr = addr - 0x4000;
		} else if (addr < 0xC000) {
			src = frames[2];
			realAddr = addr - 0x8000;
		} else if (addr < 0xE000) {
			src = frames[3];
			realAddr = addr - 0xA000;
		} else if (addr < 0x10000) {
			src = frames[3];
			realAddr = addr - 0xC000;
		} else {
			// Out of bounds
			return null;
		}
		return Arrays.copyOfRange(src, realAddr, realAddr + length);
	}

	@Override
	public String[] getRegionNames() {
		return new String[]{
				"Z80", "ROM", "RAM", "VRAM"
		};
	}

	@Override
	public long getRegionLength(int region) {
		switch (region) {
			case 0: return 0x8000;
			case 1: return pages.length * 0x4000;
			case 2: return 0x2000;
			case 3: return 0x4000;
			default: return 0;
		}
	}

	@Override
	public long getRegionOffset(int region, long offset) {
		switch (region) {
			case 0: return offset;
			case 1: return offset;
			case 2: return offset + 0x8000;
			case 3: return offset + 0xC000;
			default: return 0;
		}
	}

	public int getNFrames() {
		return frames.length;
	}

	public int getNPages() {
		return pages.length;
	}

	public int getNCartRAMPages() {
		return cartRAMPages.length;
	}
}
