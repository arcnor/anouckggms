package com.arcnor.anouckmse.machine.c64;

import com.arcnor.system.MemoryChip;

import java.util.Arrays;

public class C64Memory implements MemoryChip {
	private final byte[] mem;

	private static final int OFFS_BIOS_RAM  = 0x0002;
	private static final int SIZE_BIOS_RAM  = 0x00FE;
	private static final int OFFS_STACK     = 0x0100;
	private static final int SIZE_STACK     = 0x0100;
	private static final int OFFS_BIOS_DATA = 0x0200;
	private static final int SIZE_BIOS_DATA = 0x0600;
	private static final int OFFS_BASIC_RAM = 0x0800;
	private static final int SIZE_BASIC_RAM = 0x7800;
	private static final int OFFS_BASIC     = 0xA000;
	private static final int SIZE_BASIC     = 0x2000;
	private static final int OFFS_RAM       = 0xC000;
	private static final int SIZE_RAM       = 0x1000;
	private static final int OFFS_CHAR      = 0xD000;
	private static final int SIZE_CHAR      = 0x1000;
	private static final int OFFS_KERNAL    = 0xE000;
	private static final int SIZE_KERNAL    = 0x2000;

	public C64Memory(final byte[] chargen, final byte[] basic, final byte[] kernal) {
		if (chargen.length != SIZE_CHAR) {
			throw new RuntimeException("Invalid chargen size");
		}
		if (basic.length != SIZE_BASIC) {
			throw new RuntimeException("Invalid basic size");
		}
		if (kernal.length != SIZE_KERNAL) {
			throw new RuntimeException("Invalid kernal size");
		}

		mem = new byte[0x10000];
		System.arraycopy(chargen, 0, this.mem, OFFS_CHAR, chargen.length);
		System.arraycopy(basic,   0, this.mem, OFFS_BASIC, basic.length);
		System.arraycopy(kernal,  0, this.mem, OFFS_KERNAL, kernal.length);
	}

	@Override
	public byte readSigned(int addr) {
		return mem[addr];
	}

	@Override
	public int readUnsigned(int addr) {
		return mem[addr] & 0xFF;
	}

	@Override
	public void write(int addr, byte value) {
		mem[addr] = value;
	}

	@Override
	public byte[] getData(int addr, int length) {
		return Arrays.copyOfRange(mem, addr, addr + length);
	}

	@Override
	public String[] getRegionNames() {
		return new String[] {
				"BIOS RAM", "STACK", "BIOS DATA", "BASIC RAM",
				"BASIC", "RAM", "CHAR", "KERNAL"
		};
	}

	@Override
	public long getRegionLength(int region) {
		switch (region) {
			case 0: return SIZE_BIOS_RAM;
			case 1: return SIZE_STACK;
			case 2: return SIZE_BIOS_DATA;
			case 3: return SIZE_BASIC_RAM;
			case 4: return SIZE_BASIC;
			case 5: return SIZE_RAM;
			case 6: return SIZE_CHAR;
			case 7: return SIZE_KERNAL;
			default: return -1;
		}
	}

	@Override
	public long getRegionOffset(int region, long offset) {
		switch (region) {
			case 0: return OFFS_BIOS_RAM;
			case 1: return OFFS_STACK;
			case 2: return OFFS_BIOS_DATA;
			case 3: return OFFS_BASIC_RAM;
			case 4: return OFFS_BASIC;
			case 5: return OFFS_RAM;
			case 6: return OFFS_CHAR;
			case 7: return OFFS_KERNAL;
			default: return -1;
		}
	}
}
