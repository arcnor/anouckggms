package com.arcnor.anouckmse.machine.nes;

import com.arcnor.system.MemoryChip;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Arrays;

public class NESMemory implements MemoryChip {
	private byte[] trainer;
	private byte[] prgROM;
	private byte[] prgRAM;
	private byte[] chrROM;
	private final byte[] RAM = new byte[0x800];
	private final byte[] SRAM = new byte[0x2000];
	private final byte[] regs = new byte[8];
	private final byte[] regs2 = new byte[0x20];

	/**
	 * @param rom ROM in iNES format
	 */
	public NESMemory(final byte[] rom) {
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(rom));
		byte[] tempTrainer = null;
		byte[] tempPRGROM = null;
		byte[] tempPRGRAM = null;
		byte[] tempCHRROM = null;

		try {
			if (dis.readByte() != 'N' || dis.readByte() != 'E' || dis.readByte() != 'S' || dis.readByte() != 0x1A) {
				return;
			}
			int n16Bnks = dis.readByte();
			int n8VBnks = dis.readByte();

			int flags6 = dis.readByte();
			boolean vertMirror = (flags6 & 0x01) != 0;
			boolean battBacked = (flags6 & 0x02) != 0;
			boolean hasTrainer = (flags6 & 0x04) != 0;
			boolean fourScreen = (flags6 & 0x08) != 0;
			int flags7 = dis.readByte();
			boolean vsSystem = (flags7 & 0x01) != 0;
			int mapper = (flags7 & 0xF0) | ((flags6 & 0xF0) >> 4);

			int n8Bnks = dis.readByte();
			int flag9 = dis.readByte();
			int flag10 = dis.readByte();
			for (int j = 0; j < 5; j++) {
				dis.readByte();
			}

			if (hasTrainer) {
				tempTrainer = new byte[512];
				dis.read(tempTrainer);
			}
			tempPRGROM = new byte[0x4000 * (n16Bnks > 1 ? n16Bnks : 2)];
			dis.read(tempPRGROM, 0, 0x4000 * n16Bnks);
			if (n16Bnks == 1) {
				System.arraycopy(tempPRGROM, 0, tempPRGROM, 0x4000, 0x4000);
			}
			tempCHRROM = new byte[0x1000 * (n8VBnks > 0 ? n8VBnks : 1)];
			dis.read(tempCHRROM);
			tempPRGRAM = new byte[0x1000 * (n8Bnks > 0 ? n8Bnks : 1)];
		} catch (IOException e) {
			e.printStackTrace();
		}
		trainer = tempTrainer;
		prgROM = tempPRGROM;
		chrROM = tempCHRROM;
		prgRAM = tempPRGRAM;
	}

	@Override
	public byte readSigned(int addr) {
		if (addr < 0x2000) {
			return RAM[addr & 0x07FF];
		} else if (addr < 0x4000) {
			return regs[addr & 0x0007];     // I/O registers
		} else if (addr < 0x4020) {
			return regs2[addr & 0x001F];    // I/O registers
		} else if (addr < 0x6000) {         // Expansion ROM
			return 0;
		} else if (addr < 0x8000) {
			return SRAM[addr - 0x6000];
		} else if (addr < 0x10000) {
			return prgROM[addr - 0x8000];
		}
		return 0;
	}

	@Override
	public int readUnsigned(int addr) {
		return readSigned(addr) & 0xFF;
	}

	@Override
	public void write(int addr, byte value) {
		if (addr < 0x2000) {
			RAM[addr & 0x07FF] = value;
		} else if (addr < 0x4000) {
			regs[addr & 0x0007] = value;    // I/O registers
		} else if (addr < 0x4020) {
			regs2[addr & 0x001F] = value;   // I/O registers
		} else if (addr < 0x6000) {         // Expansion ROM
		} else if (addr < 0x8000) {
			SRAM[addr - 0x6000] = value;
		} else if (addr < 0x10000) {
			prgROM[addr - 0x8000] = value;
		}
	}

	@Override
	public byte[] getData(int addr, int length) {
		if (addr < 0x2000) {
			int newAddr = addr & 0x07FF;
			return Arrays.copyOfRange(RAM, newAddr, newAddr + length);
		} else if (addr < 0x4000) {
			int newAddr = addr & 0x0007;    // I/O registers
			return Arrays.copyOfRange(regs, newAddr, newAddr + length);
		} else if (addr < 0x4020) {
			int newAddr = addr & 0x001F;    // I/O registers
			return Arrays.copyOfRange(regs2, newAddr, newAddr + length);
		} else if (addr < 0x6000) {    // Expansion ROM
		} else if (addr < 0x8000) {
			int newAddr = addr - 0x6000;
			return Arrays.copyOfRange(SRAM, newAddr, newAddr + length);
		} else if (addr < 0x10000) {
			int newAddr = addr - 0x8000;
			return Arrays.copyOfRange(prgROM, newAddr, newAddr + length);
		}
		return new byte[length];  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public String[] getRegionNames() {
		return new String[] {
				"ZERO", "STACK", "RAM", "REGS", "REGS2", "CARTRAM", "LROM", "HROM"
		};
	}

	@Override
	public long getRegionLength(int region) {
		switch (region) {
			case 0: return 0x0100;
			case 1: return 0x0100;
			case 2: return 0x0600;
			case 3: return regs.length;
			case 4: return regs2.length;
			case 5: return 0x2000;
			case 6: return 0x4000;
			case 7: return 0x4000;
			default: return -1;
		}
	}

	@Override
	public long getRegionOffset(int region, long offset) {
		switch (region) {
			case 0: return 0x0000;
			case 1: return 0x0100;
			case 2: return 0x0200;
			case 3: return 0x2000;
			case 4: return 0x4000;
			case 5: return 0x6000;
			case 6: return 0x8000;
			case 7: return 0xC000;
			default: return -1;
		}
	}
}
