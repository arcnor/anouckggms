package com.arcnor.anouckmse.machine.atari2600;

import com.arcnor.system.MemoryChip;

import java.util.Arrays;

public class Atari2600Memory implements MemoryChip {
	private final byte[] mem;

	public Atari2600Memory(final byte[] mem) {
		if (mem.length != 0x1000) {
			throw new RuntimeException("Unsupported memory size (" + mem.length + ")");
		}
		this.mem = new byte[0x10000];
		System.arraycopy(mem, 0, this.mem, 0xF000, mem.length);
	}

	@Override
	public byte readSigned(int addr) {
		return mem[addr];
	}

	@Override
	public int readUnsigned(int addr) {
		return mem[addr] & 0xFF;
	}

	@Override
	public void write(int addr, byte value) {
		mem[addr] = value;
	}

	@Override
	public byte[] getData(int addr, int length) {
		return Arrays.copyOfRange(mem, addr, addr + length);
	}

	@Override
	public String[] getRegionNames() {
		return new String[]{"0 PAGE", "STACK", "RAM", "IO", "ROM", "VECTOR"};  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public long getRegionLength(int region) {
		switch (region) {
			case 0: return 0x0100;
			case 1: return 0x0100;
			case 2: return 0x3E00;
			case 3: return 0x4000;
			case 4: return 0x7FF9;
			case 5: return 0x0006;
			default: return 0;
		}
	}

	@Override
	public long getRegionOffset(int region, long offset) {
		switch (region) {
			case 0: return 0x0000;
			case 1: return 0x0100;
			case 2: return 0x0200;
			case 3: return 0x4000;
			case 4: return 0x8000;
			case 5: return 0xFFFA;
			default: return 0;
		}
	}
}
