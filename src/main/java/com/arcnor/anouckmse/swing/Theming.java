package com.arcnor.anouckmse.swing;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.plaf.metal.MetalLookAndFeel;
import java.awt.*;
import java.util.ArrayList;

public class Theming {
	public static final Color COLOR_FORE = new Color(169, 183, 198);
	public static final Color COLOR_FORE_MENU = new Color(186, 186, 186);

	public static final Color COLOR_BACK_WINDOW = new Color(43, 43, 43);
	public static final Color COLOR_BACK_MENU = new Color(60, 63, 65);
	public static final Color COLOR_BACK_SELECTION_MENU = new Color(75, 110, 175);
	public static final Color COLOR_BACK_BUTTON_TOP = new Color(0x535657);
	public static final Color COLOR_BACK_BUTTON_BOTTOM = new Color(0x444748);

	public static final Color COLOR_BORDER_BUTTON = new Color(0x595A5A);
	public static final Color COLOR_BORDER_BOTTOM = new Color(85, 85, 85);
	public static final Color COLOR_BORDER_TOP = new Color(40, 40, 40);

	public static final Color COLOR_LINE = new Color(77, 77, 77);
	public static final Color COLOR_SELECTION = new Color(33, 66, 131);
	public static final Color COLOR_STRING = new Color(165, 194, 92);
	public static final Color COLOR_FIELD = new Color(152, 118, 170);
	public static final Color COLOR_RESERVED = new Color(204, 120, 50);

	public static final CompoundBorder BORDER_TOP = BorderFactory.createCompoundBorder(
			BorderFactory.createMatteBorder(1, 0, 0, 0, COLOR_BORDER_TOP),
			BorderFactory.createMatteBorder(1, 0, 0, 0, COLOR_BORDER_BOTTOM)
	);
	public static final CompoundBorder BORDER_BUTTON = BorderFactory.createCompoundBorder(
			BorderFactory.createLineBorder(COLOR_BORDER_BUTTON, 1),
			BorderFactory.createEmptyBorder(2, 4, 2, 4)
	);

	public static void loadLAF() {
		try {
			UIManager.setLookAndFeel(MetalLookAndFeel.class.getName());
			UIDefaults defaults = UIManager.getDefaults();
			Font fntMonospaced = new Font("Monospaced", Font.PLAIN, 12);
			Font fntMenu = new Font("Arial", Font.PLAIN, 12);

			java.util.Enumeration<Object> keys = defaults.keys();
			while (keys.hasMoreElements()) {
				Object key = keys.nextElement();
				Object value = UIManager.get(key);
				if (key != null && key instanceof String && (((String)key).startsWith("Button") || ((String) key).startsWith("Menu") || ((String) key).startsWith("CheckBoxMenu"))) {
					continue;
				}
				if (value != null && value instanceof javax.swing.plaf.FontUIResource) {
					UIManager.put(key, fntMonospaced);
				}
			}
			UIManager.put("ScrollBarUI", AnouckScrollBarUI.class.getName());
			UIManager.put("PopupMenu.border", BorderFactory.createLineBorder(COLOR_BACK_WINDOW));
			UIManager.put("Panel.background", COLOR_BACK_WINDOW);
			UIManager.put("ScrollPane.border", "");
			UIManager.put("TextField.border", BorderFactory.createCompoundBorder(
					BorderFactory.createLineBorder(COLOR_BORDER_TOP),
					BorderFactory.createEmptyBorder(2,5,2,5))
			);
			UIManager.put("TextField.foreground", COLOR_FORE);
			UIManager.put("TextField.background", COLOR_BACK_MENU);
			UIManager.put("TextField.caretForeground", COLOR_FORE);
			UIManager.put("Button.font", fntMenu);
			ArrayList<Object> value = new ArrayList<Object>();
			value.add(0.5);
			value.add(0.0);
			value.add(COLOR_BACK_BUTTON_TOP);
			value.add(COLOR_BACK_BUTTON_BOTTOM);
			value.add(Color.BLACK);
			UIManager.put("Button.gradient", value);
			UIManager.put("Button.border", BORDER_BUTTON);
			UIManager.put("Button.foreground", COLOR_FORE);
			Color brighter = COLOR_BACK_BUTTON_TOP.brighter();
			UIManager.put("Button.select", brighter);
			UIManager.put("Button.shadow", brighter);
			UIManager.put("Button.highlight", brighter);
			UIManager.put("Button.select", brighter);
			UIManager.put("Button.focus", brighter);
			UIManager.put("Label.foreground", COLOR_FORE);
			String[] menuKeys = {"Menu", "MenuBar", "MenuItem", "CheckBoxMenuItem", "RadioButtonMenuItem"};
			for (String menuKey : menuKeys) {
				UIManager.put(menuKey + ".font", fntMenu);
				UIManager.put(menuKey + ".borderPainted", Boolean.FALSE);
				UIManager.put(menuKey + ".background", COLOR_BACK_MENU);
				UIManager.put(menuKey + ".foreground", COLOR_FORE_MENU);
				UIManager.put(menuKey + ".selectionBackground", COLOR_BACK_SELECTION_MENU);
				UIManager.put(menuKey + ".selectionForeground", COLOR_FORE_MENU);
			}
		} catch(Exception e) {
			System.out.println("Error setting native LAF: " + e);
		}
	}
}
