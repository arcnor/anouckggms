package com.arcnor.anouckmse.swing;

import javax.swing.*;

public interface MutableListModel<T> extends ListModel {
	public boolean add(T obj);
	public boolean contains(T obj);
	public boolean remove(T obj);
	public boolean remove(int index);
}
