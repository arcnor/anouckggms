package com.arcnor;

import com.arcnor.system.MemoryChip;

import java.util.Arrays;

public class SimpleMemory implements MemoryChip {
	private final byte[] mem;

	public SimpleMemory(int size) {
		mem = new byte[size];
	}

	@Override
	public byte readSigned(int addr) {
		return mem[addr];
	}

	@Override
	public int readUnsigned(int addr) {
		return mem[addr] & 0xFF;
	}

	@Override
	public void write(int addr, byte value) {
		mem[addr] = value;
	}

	@Override
	public byte[] getData(int addr, int length) {
		return Arrays.copyOfRange(mem, addr, addr + length);
	}

	@Override
	public String[] getRegionNames() {
		return new String[] { "MEM" };
	}

	@Override
	public long getRegionLength(int region) {
		return mem.length;
	}

	@Override
	public long getRegionOffset(int region, long offset) {
		return offset;
	}
}
